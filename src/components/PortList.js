import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import {API_URL,getToken} from './constants';
import {  Redirect } from  'react-router-dom';
//import TrashIcon from '@material-ui/svg-icons/action/delete'

class PortList extends React.Component{
    constructor(props){
        super(props);
        this.state={
          items:  [],
          isloaded: false,
          pid:'',
          message:'',
          open:false,
          ip:'',
          isdeleted:false,
          return:false
      }
      }

      handleDelete = (event) =>{
        event.preventDefault();
       
        this.setState({open:!this.state.open}) 
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.delete(API_URL+"/killprocess?ip="+this.state.ip+"&pid="+this.state.pid,{headers: {'Authorization': AuthStr}})
        .then(res => {
          window.location.reload(true);
            if(res.status === 200){
            this.setState({isdeleted:!this.state.isdeleted})
            this.setState({message:res.data.data[0].message})
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
      }

      handleDailog = parameter => event =>{
        event.preventDefault();
        this.setState({pid:parameter})
        this.setState({open:!this.state.open}) 
      }
      handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }
            
        componentDidMount() {
            
            this.state.ip=this.props.location.state.ip
            let TOKEN = getToken();
            const AuthStr = 'Bearer '.concat(TOKEN); 
            axios.get(API_URL+'/portlist/'+this.props.location.state.ip,{ headers: {'Authorization': AuthStr}})
            .then(res => {
              if(res.status === 200){
              //alert(res.data.data[0].message);
            
            this.setState({items:res.data.data})
            }
            if(res.status === 201){
              this.setState({message:res.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
          
              this.setState({return:!this.state.return})
          }
            })     
        
        }
      render() {
        if(this.state.return){
          return <Redirect to="/signin"/>
      }
        var {items}=this.state;
        return (
          
          <TableContainer>
            <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               {this.state.ip}  
        </Typography>
            <Table aria-label="customized table">
            <TableHead>
            <TableRow>
            <TableCell>Sl.No</TableCell>
              <TableCell align="center">Protocol</TableCell>
              <TableCell align="center">Recv-Q</TableCell>
              <TableCell align="center">Send-Q&nbsp;</TableCell>
              <TableCell align="center">Local Address&nbsp;</TableCell>
              <TableCell align="center">Foreign Address&nbsp;</TableCell>
              <TableCell align="center">State&nbsp;</TableCell>
              <TableCell align="center">PID/Program&nbsp;</TableCell>
              <TableCell align="center">Kill process&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map((item,index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">{index+1}</TableCell>
                <TableCell align="center">
                {item.proto}                 
                </TableCell>
                <TableCell align="center">{item.recv}</TableCell>
                <TableCell align="center">{item.send}</TableCell>
                <TableCell align="center">{item.laddress}</TableCell>
                <TableCell align="center">{item.faddress}</TableCell>       
                <TableCell align="center">{item.state}</TableCell> 
              <TableCell align="center">{item.pid}</TableCell>     
              <TableCell align="center">
                {/* <DailogOpen ip={item.ip} /> */}
              <DeleteIcon fontSize="small" onClick={this.handleDailog(item.pid)}/>
              </TableCell>

              <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Status"}</DialogTitle>
          <DialogContent>
            
            <DialogContentText>
            Do You Want To Delete The Process : {this.state.pid}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         <Button onClick={this.handleClose} color="primary">
             No
          </Button>
          <Button onClick={this.handleDelete} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>
              </TableRow>
              
            ))}
          </TableBody>
            </Table>
          {this.state.isdeleted && <Alert severity="success">Process Killed SuccessFully</Alert> }
          </TableContainer>
        );
      }
     }
    
export default PortList;