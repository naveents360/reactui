import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,getToken,validateipaddress,validatepassword,validateportnumber,validateusernamelength} from './../constants';
import {Redirect } from  'react-router-dom';

class Telnet extends React.Component{
    constructor(props){
        super(props);
        this.state={
        serverip:'',
        username:'',
        password:'',
        remoteserverip:'',
        Port:'',
        message:'',
        userhelptext:'',
        iphelptext:'',
        porthelptext:'',
        passhelpertext:'',
        open:false,
        loading:false,
        return:false
        }
       }

       handleHomeButton = event =>{
        event.preventDefault();
        this.props.history.push('/home')
       }

       handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }

       handleChangeserverip = event =>{
           event.preventDefault();
           this.setState({serverip: event.target.value})
       }
       handleChangeusername= event =>{
           event.preventDefault();
           this.setState({username:event.target.value})
       }
       handleChangepassword = event=>{          
           event.preventDefault();
           this.setState({password:event.target.value})
           }

       handleChangeremoteserverip = event =>{
           event.preventDefault();
           this.setState({remoteserverip:event.target.value})
       }
       handleChangeportnumber = event =>{
           this.setState({Port:event.target.value})
           event.preventDefault();
           
       }

       validate = ()=>{
           var port = this.state.Port
           const ip = this.state.serverip;
           const remip = this.state.remoteserverip;
           let passhelpertext = validatepassword(this.state.password)
           let iphelptext = validateipaddress(ip)
           let userhelptext = validateusernamelength(this.state.username)  
           let remoteiphelptext = validateipaddress(remip)
           let porthelptext = validateportnumber(port) 
             if(userhelptext || iphelptext ||  passhelpertext || porthelptext || remoteiphelptext){
               this.setState({userhelptext,iphelptext,passhelpertext,porthelptext,remoteiphelptext});
                return false;
            }
            return true;
       }
        handleClick(event){
        event.preventDefault();

            const payload = {
               "port":this.state.Port,
               "username":this.state.username,
               "password":this.state.password,
               "ip":this.state.serverip,
               "remoteserver":this.state.remoteserverip
           } 
        const isvalid = this.validate();
        if(isvalid){    
            this.setState({open: !this.state.open })
            this.setState({loading: !this.state.loading})
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.post(API_URL+'/telnetport', payload, { headers: {'Authorization': AuthStr}})
        .then(res => {
            this.setState({loading: !this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.data[0].message})
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
            if(error.response){
                this.setState({return:!this.state.return})
            }
          })     
          this.setState({username:'',Port:'',password:'',serverip:'',remoteserverip:'',userhelptext:'',iphelptext:'',remoteiphelptext:'',passhelpertext:''})
        }
        
         }
    render(){
        const disabled = !this.state.serverip.length || !this.state.Port.length || !this.state.password.length || !this.state.remoteserverip.length || !this.state.username.length;
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Telnet
        </Typography>
        <form  noValidate>
          <TextField           
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Server IP Address"
            name="serverip"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.serverip}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Username"
            name="username"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.username}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            onChange = {this.handleChangepassword}
            value = {this.state.password}
            helperText = {this.state.passhelpertext}
            />
            <br/>
             <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Remote Server IP Address"
            name="remoteserverip"
            autoFocus
            onChange = {this.handleChangeremoteserverip}
            helperText = {this.state.remoteiphelptext}
            value = {this.state.remoteserverip}
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Port Number"
            name="Port Number"
            autoFocus
            onChange = {this.handleChangeportnumber}
            value={this.state.Port}
            helperText ={this.state.porthelptext}
          />
           <br/>
           
          <Button
            disabled={disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}            
            >
            Submit            
           </Button>
           <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleHomeButton(event)}            
            >
            Home            
           </Button>
          <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary">
             OK
            </Button>}
          </DialogActions>
        </Dialog>
        </form>
        </Container>
    )

}
}

export default Telnet;