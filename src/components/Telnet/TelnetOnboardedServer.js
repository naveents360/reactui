import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,getToken,validateipaddress,validateportnumber} from './../constants';
import {Redirect } from  'react-router-dom';

class TelnetOnboardedServer extends React.Component{
    constructor(props){
        super(props);
        this.state={
        serverip:'',
        remoteserverip:'',
        Port:'',
        message:'',
        iphelptext:'',
        porthelptext:'',
        passhelpertext:'',
        open:false,
        loading:false,
        return:false
        }
       }

       handleHomeButton = event =>{
        event.preventDefault();
        this.props.history.push('/home')
       }
       
       handleClose = event =>{
           event.preventDefault();
           this.setState({open: !this.state.open })
       }
       handleChangeserverip = event =>{
           event.preventDefault();
           this.setState({serverip: event.target.value})
       }
       handleChangeremoteserverip = event =>{
           event.preventDefault();
           this.setState({remoteserverip:event.target.value})
       }
       handleChangeportnumber = event =>{
           this.setState({Port:event.target.value})
           event.preventDefault();  
       }

       validate = ()=>{
            var port = this.state.Port
           const ip = this.state.serverip;
           const remip = this.state.remoteserverip;
           let iphelptext = validateipaddress(ip) 
           let remoteiphelptext = validateipaddress(remip)
           let porthelptext = validateportnumber(port) 
             if( iphelptext ||  porthelptext || remoteiphelptext){
               this.setState({iphelptext,porthelptext,remoteiphelptext});
                return false;
            }
            return true;
       }
       
        handleClick(event){
        event.preventDefault();
            const payload = {
               "port":this.state.Port,
               "ip":this.state.serverip,
               "remoteserver":this.state.remoteserverip
           } 
        const isvalid = this.validate();
        if(isvalid){    
            this.setState({open: !this.state.open })
            this.setState({loading: !this.state.loading})
        this.setState({message:''})    
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.post(API_URL+'/telnetonboardedserver', payload, { headers: {'Authorization': AuthStr}})
        .then(res => {
            this.setState({loading:!this.state.loading})
            if(res.status === 200){
            //this.setState({loading:!this.state.loading})
            this.setState({message:res.data.data[0].message})
          }
          else if(res.status === 201){
              
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
            if(error.response){
                this.setState({return:!this.state.return})
            }
          })     
          this.setState({Port:'',serverip:'',remoteserverip:'',iphelptext:'',remoteiphelptext:''})
        }
        
         }
    render(){
        const disabled = !this.state.serverip.length || !this.state.Port.length || !this.state.remoteserverip.length;
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Telnet OnBoarded Server
        </Typography>
        <form  noValidate>
          <TextField           
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Server IP Address"
            name="serverip"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.serverip}          
          />
            <br/>
             <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Remote Server IP Address"
            name="remoteserverip"
            autoFocus
            onChange = {this.handleChangeremoteserverip}
            helperText = {this.state.remoteiphelptext}
            value = {this.state.remoteserverip}
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Port Number"
            name="Port Number"
            autoFocus
            onChange = {this.handleChangeportnumber}
            value={this.state.Port}
            helperText={this.state.porthelptext}
          />
           <br/>
           
          <Button
            disabled={disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}            
            >
            Submit            
           </Button>
           <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleHomeButton(event)}            
            >
            Home            
           </Button>
           
           <Dialog
          //fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary">
             OK
            </Button>}
          </DialogActions>
        </Dialog>
        </form>
        </Container>
    )

}
}
export default TelnetOnboardedServer;