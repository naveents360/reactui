
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import axios from 'axios';
import Tooltip from '@material-ui/core/Tooltip';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Container from '@material-ui/core/Container';

class StatusUdate extends React.Component{
    constructor(props){
        super(props);
        this.state={
            approvedby:'',
            approvallevel:'',
            servicetype:'',
            requestedby:'',
            }
    }
    
    componentDidMount(){
      this.setState({requestedby : this.props.location.state.requestedby})
      this.setState({servicetype : this.props.location.state.servicetype})
    }
    handleChangeapprovedby = event =>{
        event.preventDefault();
        this.setState({approvedby: event.target.value})
    }
    
    handleapprovallevel= event =>{
        event.preventDefault();
        this.setState({approvallevel:event.target.value})
    }

    render(){
        const disabled = !this.state.approvedby.length;
      return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              Approval Page
        </Typography>
        <form  noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Approved By"
            name="Approved by"      
            autoFocus
            onChange = {this.handleChangeapprovedby} 
            value={this.state.approvedby}          
          />
          <br/>
          <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Approval Level</InputLabel>
        <Select          
          value={this.state.approvallevel}
          onChange={this.handleapprovallevel}
          label="Approval Level"
        >
          <MenuItem value="L1Approval">L1 Approval</MenuItem>
          <MenuItem value="L2Approval">L2 Approval</MenuItem>
          <MenuItem value="L3Approval">L3 Approval</MenuItem>
          <MenuItem value="L4Approval">L4 Approval</MenuItem>
          <MenuItem value="L5Approval">L5 Approval</MenuItem>         
        </Select>
      </FormControl>
       <br/>
          
          <TextField
          disabled="True"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="Service Type"
            label="Service Type"
            onChange = {this.handleChangeservicetype}
            value = {this.state.servicetype}
            />
            <br/>
             <TextField
             disabled="True"
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Requested By"
            name="requestedby"
            autoFocus
            value = {this.state.requestedby}
          />
          <br/>
           <Tooltip title="Approve">
          <Button
            disabled={disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}            
            >
            Approve            
           </Button> 
          </Tooltip>
        </form>
        </Container>
        
      )
    }
  }

  export default StatusUdate;