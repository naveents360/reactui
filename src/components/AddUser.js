import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken,ADMIN,TEAMLEAD} from './constants';
import { Redirect } from  'react-router-dom';


class AddUser extends React.Component{
    constructor(props){
        super(props);
        this.state={
        username:'',
        message:'',
        userhelptext:'',
        iphelptext:'',
        project:'',
        role:'',
        open:false,
        loading:false,
        projects:[],
        return:false,
        roles:[],
        }     
       }
       componentDidMount(){
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getownership', { headers: {'Authorization': AuthStr}})      
        .then(response => {
           let ownership = response.data.ownership;
           //console.log(ownership)
            if(ownership === "admin"){
                this.setState({roles:ADMIN})
            }
            if(ownership === "teamlead"){
               this.setState({roles:TEAMLEAD})
           }
          
         })
        .catch((error) => {
           if(error.response){
               //console.log(error.response)
               this.setState({return:!this.state.return})
           }
         });
         axios.get(API_URL+'/getprojectlist', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
    if(error.response){
        if(error.response){
            this.setState({return:!this.state.return})
        }
     }
  });
    }


       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }
       handleRole = event =>{
        event.preventDefault();
        this.setState({role: event.target.value})
    }

       handleProject = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({project: event.target.value})
    }


       handleChangeusername= event =>{
           event.preventDefault();
           this.setState({username:event.target.value})
       }

        handleClick(event){
        event.preventDefault();
        this.setState({open: !this.state.open })
        this.setState({loading: !this.state.loading})
            const payload = {
               "username":this.state.username,
               "is_active":"True",
               "projects":[this.state.project],
               "ownership":this.state.role
           } 
        //console.log(payload)
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        axios.post(API_URL+'/onboarduser', payload,options)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.message})           
          }
          else if(res.status === 201){
              //console.log(res.data.message);
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            //console.log(error.response)
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({username:'',role:'',project:''})
        }
        

         
        
    render(){
      const disabled =  !this.state.role.length  || !this.state.project.length || !this.state.username.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Onboard User
        </Typography>
        <form  noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Username"
            name="username"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.username}
          />
    <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Project</InputLabel>
        <Select          
          value={this.state.project}
          onChange={this.handleProject}
          label="Project"
        >
          {
            this.state.projects.map((project,index)=>
            <MenuItem key={index} value={project}>{project}</MenuItem>)
          }
        </Select>
      </FormControl>
      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Role</InputLabel>
        <Select          
          value={this.state.role}
          onChange={this.handleRole}
          label="Role"
        >
         {
            this.state.roles.map((project,index)=>
            <MenuItem  key={index} value={project}>{project}</MenuItem>)
          }
          {/* <MenuItem value="REPLICA">REPLICA</MenuItem>     */}
        </Select>
      </FormControl>
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Submit            
           </Button>                
            <br/>            
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default AddUser;