import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken,encryptdata} from './../constants';
import { Redirect } from  'react-router-dom';
//import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';


class InstallJava extends React.Component{
    constructor(props){
        super(props);
        this.state={
        serverip:'',
        message:'',
        userhelptext:'',
        iphelptext:'',
        version:'',
        env:'',
        open:false,
        loading:false,
        versions:[],
        return:false
        }     
       }
       componentDidMount(){
        this.setState({ip : this.props.location.state.ip})
        this.setState({currentversion : "Checking..."})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getjavasoftwareinfo', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     this.setState({versions:response.data.msg.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
  const payload = {
    "ip":this.props.location.state.ip,
    "command":"java -version 2>&1 >/dev/null | grep 'java version' | awk '{print $3}'"
} 
  axios.post(API_URL+'/executecommand', payload,{ headers: { 'Authorization': AuthStr } })
        .then(res => {
            if(res.status === 200){
              console.log("Hello")
              console.log(res.data.data[0].output)
              if (res.data.data[0].output === "Success"){
                this.setState({currentversion : "JDK Not Installed"})
              }
              else{
            this.setState({currentversion:res.data.data[0].output})          
              }
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
              console.log("hello")
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
    }
       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }

       handleChangecurrentversion = event => {
           event.preventDefault();
           this.setState({currentversion: event.target.value})
       }

       handleversion = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({version: event.target.value})
    }


      
        handleClick(event){
        event.preventDefault();
        this.setState({open: !this.state.open })
        this.setState({loading: !this.state.loading})
            const payload = {
               "ip":this.state.ip,
               "version":this.state.version
           } 
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        axios.post(API_URL+'/javainstall', payload,options)
        .then(res => {
          this.setState({loading: !this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.message})           
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
          
        })
        .catch(error => {
          if(error.response){            
            this.setState({return:!this.state.return})
        }
          })     
          
          this.setState({currentversion:this.state.version})
        }
        
              
        
    render(){
      const disabled = !this.state.version.length || !this.state.currentversion.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Install Java on {this.state.ip}
        </Typography>
        <form  noValidate>
        <TextField     
        //disabled      
        variant="outlined"
        margin="normal"
        fullWidth
        required
        label="Current version"
        name="Current version"      
        autoFocus
        //onChange = {this.handleChangecurrentversion}
        value={this.state.currentversion}          
      />
        <br/>
    <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Versions Available</InputLabel>
        <Select          
          value={this.state.version}
          onChange={this.handleversion}
          label="Available Versions"
        >
          {
            this.state.versions.map((version,index)=>
            <MenuItem key={index} value={version}>{version}</MenuItem>)
          }
        </Select>
      </FormControl>  
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Install Java            
           </Button>                
            <br/>        
          <p>
            Note:- Please Restart Java Applications After JDK Upgrade.
            Please Do Not Upgrade Java On Kafka Server
          </p>
            
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default InstallJava;