import React, { Component } from 'react';
import ChatBot from 'react-simple-chatbot';
import Onboard from './../Onboard'

//import Review from './Review';
import { ThemeProvider } from "styled-components";

class SimpleForm extends Component {
    render() {
      return (

        <ChatBot
          steps={[
            {
              id: '1',
              message: 'Hello! How Can I Help You. Please Select a option below.',
              trigger: 'mainoptions',
            },
            {
              id: 'mainoptions',
              options: [
                { value: 'dls', label: 'Team DL', trigger: '5' },
                // { value: 'urls', label: 'Links', trigger: 'urls' },
                //{ value: 'female', label: 'Female', trigger: '5' },
              ],
            },
            {
                id: 'urls',
                options: [
                  { value: 'hpsm', label: 'HPSM', trigger: 'hpsm' },
                  { value: 'uam', label: 'UAM', trigger: 'uam' },
                  { value: 'internalssl', label: 'Internal SSL', trigger: 'internalssl' },
                  { value: 'user privilege', label: 'User Privilege', trigger: 'userprivilege' },
                  { value: 'ci search', label: 'CI Search', trigger: 'cisearch' },
                  { value: 'smtp', label: 'SMTP', trigger: 'smtp' },
                ],
              },
              {
                id: 'hpsm',
                // component : <demodata />,
                // asMessage : true,
                message: "http://hpsmbss.oss.jio.com/smbss/index.do?lang=en",
                trigger: '1',
              },        {
                id: 'uam',
                message: "    https://jioit.ril.com/JioUserAssetRequest.aspx",
                trigger: '1',
              },        {
                id: 'internalssl',
                message: "https://r-assure.ril.com/RSAarcher/apps/ArcherApp/Home.aspx",
                trigger: '1',
              },        {
                id: 'userprivilege',
                message: "https://tops.jio.com/isecurity/",
                trigger: '1',
              },        {
                id: 'cisearch',
                message: "https://topssecurity.jio.com/cisearch/",
                trigger: '1',
              },        {
                id: 'smtp',
                message: "https://idciris.rjil.ril.com/iris-v2/iris-v2#/login",
                trigger: '1',
              },           
            {
              id: '5',
              options: [
                { value: 'kafka', label: 'Kafka', trigger: 'kafkareply' },
                { value: 'redis', label: 'Redis', trigger: 'redisreply' },
                { value: 'mysql', label: 'Mysql', trigger: 'mysqlreply' },
                { value: 'madiadb', label: 'MariaDB', trigger: 'mysqlreply' },
                { value: 'Mongodb', label: 'Mongodb', trigger: 'redisreply' },
                { value: 'cassandra', label: 'Cassandra', trigger: 'redisreply' },
                { value: 'postgresql', label: 'Postgresql', trigger: 'redisreply' },
                { value: 'nginx', label: 'Nginx', trigger: 'weblogic' },
                { value: 'java', label: 'Java', trigger: 'weblogic' },
                { value: 'activemq', label: 'ActiveMq', trigger: 'weblogic' },
                { value: 'apache tomcat', label: 'Apache Tomcat', trigger: 'weblogic' },
                { value: 'network', label: 'Network', trigger: 'network' },
                { value: 'unix', label: 'Unix', trigger: 'unix' },
                { value: 'oracle', label: 'Oracle', trigger: 'oracle' },
                { value: 'HPSM', label: 'HPSM', trigger: 'hpsm' },

                //{ value: 'female', label: 'Female', trigger: '5' },
              ],
            },
            {
              id: 'oracle',
              message: "RJIL.TOPSDBA@ril.com",
              trigger: '1',
            },
            {
              id: 'hpsm',
              message: "RJIL.OSSOpsHPSM@ril.com",
              trigger: '1',
            },

            {
                id: 'unix',
                options: [
                  { value: 'idc', label: 'IDC', trigger: 'idcunixreply' },
                  { value: 'tops', label: 'TOPS', trigger: 'topsunixreply' },
                ],
              },
              {
                id: 'idcunixreply',
                message: "RJIL.IDCNOCUnix@ril.com",
                trigger: '1',
              },            {
                id: 'topsunixreply',
                message: "RJIL.TopsUnix@ril.com",
                trigger: '1',
              },
            {
              id: 'kafkareply',
              message: "Jio.TopsHadoopAdmin@ril.com",
              trigger: '1',
            },
            {
                id: 'network',
                options: [
                  { value: 'idc', label: 'IDC', trigger: 'idcreply' },
                  { value: 'jaws', label: 'JAWS', trigger: 'jawsreply' },
                ],
              },
              {
                id: 'idcreply',
                message: "RJIL.IDCNOCNetwork@ril.com",
                trigger: '1',
              },
              {
                id: 'jawsreply',
                message: "JioJAWS.NetworkDevOps@ril.com",
                trigger: '1',
              },
            {
                id: 'redisreply',
                message: "Jio.TopsNOSQL@ril.com",
                trigger: '1',
              },
              {
                id: 'mysqlreply',
                message: "Jio.TopsMYSQL@ril.com",
                trigger: '1',
              },
              {
                  id:'weblogic',
                  message:"RJIL.TOPSWeblogic@ril.com",
                  trigger:'1',
              },
            
            {
              id: 'end-message',
              message: 'Thanks! Your data was submitted successfully!',
              end: true,
            },
          ]}
        />
       
      );
    }
  }
  
  export default SimpleForm;