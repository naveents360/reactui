import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link, Redirect } from  'react-router-dom';
import {API_URL,getToken} from './constants';
import axios from 'axios';
import SimpleForm from './Chat Bot/SimpleForm'
import './Home.css'

class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
        isadmin:false,
        ismember:true,
        isteamlead:true,
        return:false,
        showChat:false,
        setShowChat:false
        //ismember:false
        }
       }

       startChat(event){
           this.setState({showChat:!this.state.showChat})
       }
       hideChat(event){
        this.setState({showChat:!this.state.showChat})
       }

       handleClick(event){
        sessionStorage.removeItem('accesstoken');
           this.props.history.push('/signin')
       }

    componentDidMount(){
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
       axios.get(API_URL+'/getownership', { headers: {'Authorization': AuthStr}})      
 .then(response => {
    let ownership = response.data.ownership;
    
     if(ownership === "admin"){
         this.setState({isadmin:!this.state.isadmin})
     }
     if(ownership === "member"){
        this.setState({ismember:!this.state.ismember})
    }
    else{
        this.setState({isteamlead:!this.state.isteamlead})
    }
   
  })
 .catch((error) => {
    if(error.response){
        this.setState({return:!this.state.return})
    }
  });
    }

    render(){
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
        return(            
            <Container component="main" maxWidth="xs"style={{textAlign:"center"}}>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Infra Portal
            </Typography>
            <Button><Link to="/onboard">OnboardServer</Link></Button>
            <br />
            <Button><Link to="/projectlink">ProjectList</Link></Button>
            <br />
            <Button><Link to="/credentialslink">Credentials</Link></Button>
            <br />            
            <Button><Link to="/telnetlinks">Telnet</Link></Button>
            <br />
            <Button><Link to="/portinfolinks">PortInfo</Link></Button>
            <br />
            <Button><Link to="/onboardportmonitor">Onboard Ports For monitor</Link></Button>
            <br />
            <Button><Link to="/projectmonitorlink">Port Monitor Status</Link></Button>
            <br />
            <Button><Link to="/csrlinks">CSR</Link></Button>
            <br />
            <Button><Link to="/otherservicespage">Other Services</Link></Button>
            <br />
            <Button><Link to="/users">Users</Link></Button>
            <br />
            <Button><Link to="/linksmainpage">Links and SOP's</Link></Button>
            <br />
            {this.state.ismember ? <Button><Link to="/adduser">AddUser</Link></Button> :<Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            LOGOUT            
           </Button>}
            <br />
            {this.state.isteamlead ? <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            LOGOUT            
           </Button>:<Button><Link to="/servercredentials">Server Credentials</Link></Button>}
            <br />
            {/* <br/>
            {this.state.isadmin && <Button><Link to="/servercredentials">Server Credentials</Link></Button>}
            <br /> */}
            {this.state.isadmin && <Button><Link to="/table">Complete ServerList</Link></Button>}
            <br />
            {this.state.isadmin && <Button><Link to="/completeusers">Complete Users</Link></Button>}
            <br />
            {this.state.isadmin &&
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            LOGOUT            
           </Button>
            }
            <div className = "bot">
        <div style ={{display: this.state.showChat ? "" : "none"}}>
          <SimpleForm></SimpleForm>
        </div>      
        {/* <div> {this.state.showChat ? <SimpleForm></SimpleForm> : null} </div> */}
        <div>
          {!this.state.showChat 
            ? <Button  type="submit"
            //fullWidth
            variant="contained"
            color="primary" onClick={(event) => this.startChat()}>ChatBot</Button> 
            : <Button  type="submit"
            //fullWidth
            variant="contained"
            color="primary" onClick={(event) => this.hideChat()}>click to hide... </Button>}
        </div>
      </div>      
            </Container>
        )
      }

}

export default Home;


