import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import { Link,Redirect } from  'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import {API_URL,getToken} from './../constants';
import UpdateCredentials from './UpdateCredentials';

class AllServerCredentials extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      open:false,
      ip:'',
      username:'',
      password:'',
      project:'',
      env:'',
      return:false,
      //updatemodal:false
  }
  }       
  

  handleDelete = (event) =>{
    event.preventDefault();
    //console.log(this.state.ip)
    this.setState({open:!this.state.open}) 
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.delete(API_URL+"/deletecredentials?ip="+this.state.ip,{headers: {'Authorization': AuthStr}})
    .then(res => {
      window.location.reload(false);
        if(res.status === 200){
        // this.setState({message:res.data.data[0].message})
        let sortedField="project";
              let sorteditems = [...res.data.data];
              sorteditems.sort((a,b)=>{ if (a[sortedField] < b[sortedField]) {
                return -1;
              }
              if (a[sortedField] > b[sortedField]) {
                return 1;
              }
              return 0;
            });
            //console.log(sorteditems); 
            //console.log(sorteditems.filter(x=> x["ip"].includes("10.159.16.101")))
            this.setState({items:sorteditems})
      }
      else if(res.status === 201){
          this.setState({message:res.data.message})
      }
    })
    .catch(error => {
      if(error.response){
        if(error.response){
            this.setState({return:!this.state.return})
        }
     }
      })     
  }

  handleDailog = parameter => event =>{
    event.preventDefault();
    this.setState({ip:parameter})
    this.setState({open:!this.state.open}) 
  }
  handleClose = event =>{
    event.preventDefault();
    this.setState({open:!this.state.open})
  }
  
  // handlemodal = event =>{
  //   event.preventDefault();
  //   //console.log("hello")
  //   this.setState({updatemodal:!this.state.updatemodal})
  // }

  // getComponent(){
  //   if(this.state.updatemodal){
  //     return <UpdateCredentials/>;
  //   }
  // }

    componentDidMount() {
      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN);  
      axios.get(API_URL+'/servercredentials', { headers: { 'Authorization': AuthStr } })
        .then(res => {
          if(res.status === 200){
          let sortedField="project";
          let sorteditems = [...res.data];
          sorteditems.sort((a,b)=>{
             if (a[sortedField] < b[sortedField]) {
            return -1;
          }
          if (a[sortedField] > b[sortedField]) {
            return 1;
          }
          return 0;
        });
        this.setState({items:sorteditems})
        }
              })
      .catch(error => {
        if(error.response){
          if(error.response){
              this.setState({return:!this.state.return})
          }
       }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
    return (
      
      <TableContainer>
        <CssBaseline />
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
            <TableCell>Sl.No</TableCell>
              <TableCell>IP</TableCell>
              <TableCell align="center">UserName</TableCell>
              {/* <TableCell align="center">Password&nbsp;</TableCell> */}
              <TableCell align="center">Project&nbsp;</TableCell>
              <TableCell align="center">Environment&nbsp;</TableCell>
              <TableCell align="center">Edit&nbsp;</TableCell>
              <TableCell align="center">Delete&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items
            .map((item,index) => (
              <TableRow key={item.ip}>
              <TableCell>{index+1}</TableCell>
                <TableCell component="th" scope="row">
                  {item.ip}
                </TableCell>
                <TableCell align="center">{item.username}</TableCell>
                {/* <TableCell align="center">{item.password}</TableCell> */}
                <TableCell align="center">{item.project}</TableCell>
                <TableCell align="center">{item.env}</TableCell>       
                <TableCell align="center">
               <Link to={{pathname:"/individualservercredentials",  state:{
                    ip : item.ip,
                    username : item.username,
                    password:item.password,
                    env:item.env,
                    project:item.project
                }}}>Edit</Link>           
                {/* <EditIcon fontsize="small" onClick={this.handlemodal}/> */}
              </TableCell>     
              <TableCell align="center">
              <DeleteIcon fontSize="small" onClick={this.handleDailog(item.ip)}/>
              </TableCell>
                {/* {this.getComponent} */}
              <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Status"}</DialogTitle>
          <DialogContent>
            
            <DialogContentText>
            Do You Want To Delete The Onboarded Server :  {this.state.ip}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         <Button onClick={this.handleClose} color="primary">
             No
          </Button>
          <Button onClick={this.handleDelete} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>   
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      
    );
  }
 }

 export default AllServerCredentials;