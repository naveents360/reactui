import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken,validateipaddress,validatepassword,validateusernamelength} from './../constants';
import { Redirect } from  'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
//import CircularProgress from '@material-ui/core/CircularProgress';



class UpdateCredentials extends React.Component{
    constructor(props){
        super(props);
        this.state={
        serverip:'',
        username:'',
        password:'',
        message:'',
        userhelptext:'',
        iphelptext:'',
        project:'',
        env:'',
        open:false,
        loading:false,
        return:false,
        projects:[]
        }     
       }
       handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }
      handleMouseDownPassword = event => {
        event.preventDefault();
        this.setState({showPassword:!this.state.showPassword})
      };

       componentDidMount() {
        //console.log(this.props.location.state)
        this.setState({serverip : this.props.location.state.ip})
        this.setState({username : this.props.location.state.username})
        this.setState({password : this.props.location.state.password})
        this.setState({env : this.props.location.state.env})
        this.setState({project : this.props.location.state.project})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getprojectlist', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
    if(error.response){
        if(error.response){
            this.setState({return:!this.state.return})
        }
     }
  });
    }


       handleEnv = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({env: event.target.value})
    }

       handleProject = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({project: event.target.value})
    }
    
       handleChangeusername= event =>{
           event.preventDefault();
           this.setState({username:event.target.value})
       }
       handleChangepassword = event=>{          
           event.preventDefault();
           this.setState({password:event.target.value})
           }

       validate = ()=>{
        var ip = this.state.serverip;
        var port = this.state.Port
        let passhelpertext = validatepassword(this.state.password)
        let iphelptext = validateipaddress(ip)
        let userhelptext = validateusernamelength(this.state.username)  
        //let porthelptext = validateportnumber(port) 
          if(userhelptext || iphelptext ||  passhelpertext ){
            //console.log(this.state.passhelpertext)
            this.setState({userhelptext,iphelptext,passhelpertext});
            //console.log(this.state.passhelpertext)
             return false;
         }
         return true;
       }
      
        handleClick(event){
        event.preventDefault();
            const payload = {
               "username":this.state.username,
               "password":this.state.password,
               "ip":this.state.serverip,
               "project":this.state.project,
               "env":this.state.env
           } 
        const isvalid = this.validate();
        //const isvalid = true
        if(isvalid){      
          this.setState({open: !this.state.open })
          this.setState({loading: !this.state.loading})  
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        //console.log(payload)
        //console.log(options)
        axios.post(API_URL+'/updatecredentials', payload,options)
        .then(res => {
          this.setState({loading: !this.state.loading})
            if(res.status === 200){
            //console.log(res.data.message)
            //alert(res.data.data[0].message);
            this.setState({message:res.data.message})
            //console.log(this.state.message);
            //console.log("Login successfull");
          }
          else if(res.status === 201){
              //console.log(res.data.message);
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
          //this.setState({username:'',Port:'',password:'',serverip:'',remoteserverip:'',userhelptext:'',iphelptext:'',remoteiphelptext:'',passhelpertext:'',env:'',project:''})
        }
        
         }

         
        
    render(){
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Update Credentials
        </Typography>
        <form  noValidate>
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Server IP Address"
            name="serverip"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.serverip}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Username"
            name="username"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.username}
          />
          {/* <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            onChange = {this.handleChangepassword}
            value = {this.state.password}
            helperText = {this.state.passhelpertext}
            /> */}
           <br/>
           <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={this.state.showPassword ? 'text' : 'password'}
            value={this.state.password}
            onChange={this.handleChangepassword}
            fullWidth
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick= {this.handleMouseDownPassword}
                  //onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
          <FormHelperText id="filled-weight-helper-text">{this.state.passhelpertext}</FormHelperText>
        </FormControl>

           <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Project</InputLabel>
        <Select          
          value={this.state.project}
          onChange={this.handleProject}
          label="Project"
        >
          {
            this.state.projects.map((project,index)=>
            <MenuItem key={index} value={project}>{project}</MenuItem>)
          }
        </Select>
      </FormControl>
      
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Update            
           </Button>
            <br/>            
            <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default UpdateCredentials;