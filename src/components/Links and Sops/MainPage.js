import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';
import {API_URL,getToken} from '../constants'
import axios from "axios";
import {Redirect} from 'react-router-dom'

class MainPage extends React.Component{
  constructor(props){
    super(props);
    this.state={
    return:false,
  }
}

  componentDidMount(){
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/testdecodejwt', { headers: {'Authorization': AuthStr}})      
    .then(response => {
        return response.status
     })
    .catch((error) => {
        if(error.response){
       this.setState({return:!this.state.return})
        }
     });
  }

    render(){
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
      return(
        <Container style={{textAlign:"center"}}>
          <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Links and SOP's
            </Typography>
            <Button><Link to="/links">Links</Link></Button>
            <br />
            <Button><Link to="/sops">SOP's</Link></Button>
            <br />
        </Container>
      )
    }
}

export default MainPage;
