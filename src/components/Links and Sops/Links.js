import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';
import {API_URL,getToken} from '../constants'
import axios from "axios";
import {Redirect} from 'react-router-dom'

class Links extends React.Component{
  constructor(props){
    super(props);
    this.state={
    return:false,
  }
}

  componentDidMount(){
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/testdecodejwt', { headers: {'Authorization': AuthStr}})      
    .then(response => {
        return response.status
     })
    .catch((error) => {
        if(error.response){
       this.setState({return:!this.state.return})
        }
     });
  }

    render(){
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
      return(
        <Container style={{textAlign:"center"}}>
          <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Links
            </Typography>
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("http://hpsmbss.oss.jio.com/smbss/index.do?lang=en","_blank")}
            >
          HPSM            
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("https://jioit.ril.com/JioUserAssetRequest.aspx","_blank")}
            >
          UAM         
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("https://r-assure.ril.com/RSAarcher/apps/ArcherApp/Home.aspx","_blank")}
            >
          Internal SSL         
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("https://tops.jio.com/isecurity/","_blank")}
            >
          User Privilege Request         
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("https://topssecurity.jio.com/cisearch/","_blank")}
            >
          CI Search         
           </Button>
           <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("https://idciris.rjil.ril.com/iris-v2/iris-v2#/login","_blank")}
            >
          SMTP         
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() =>this.props.history.push('/home')}
            >
          Home         
           </Button>
            <br />
        </Container>
      )
    }
}

export default Links;
