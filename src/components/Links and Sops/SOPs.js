import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';
import {API_URL,getToken} from '../constants'
import axios from "axios";
import {Redirect} from 'react-router-dom'

class SOPs extends React.Component{
  constructor(props){
    super(props);
    this.state={
    return:false,
  }
}

downloadfile = parameter =>{
    //console.log(parameter)
    let TOKEN = getToken();
    console.log(parameter)
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.get(API_URL+'/downloadsop/'+parameter,{ headers: {'Authorization': AuthStr}},{responseType: "blob"})
            .then(response => {
              if(response.status === 200){
              //alert(res.data.data[0].message);
              const url = window.URL.createObjectURL(new Blob([response.data]));
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", parameter);
              document.body.appendChild(link);
              link.click();
              link.remove();
            }
            if(response.status === 201){
              this.setState({message:response.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              //console.log(error.response)
              this.setState({return:!this.state.return})
          }
            })     
        
  }

  componentDidMount(){
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/testdecodejwt', { headers: {'Authorization': AuthStr}})      
    .then(response => {
        return response.status
     })
    .catch((error) => {
        if(error.response){
       this.setState({return:!this.state.return})
        }
     });
  }

    render(){
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
      return(
        <Container style={{textAlign:"center"}}>
          <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   SOP's
            </Typography>
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("http://10.159.18.32:8000/downloadsop/fqdn_form.docx/","_self")}
            >
          FQDN             
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("http://10.159.18.32:8000/downloadsop/localid_form.docx/","_self")}
            >
          User Creation          
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("http://10.159.18.32:8000/downloadsop/port_opening_form.docx","_self")}
            >
         Port Opening Request        
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() => window.open("http://10.159.18.32:8000/downloadsop/proxy_access_form.pdf/","_self")}
            >
          Proxy Whitelist Request         
           </Button>
            <br />
            <br />
            <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() =>this.props.history.push('/home')}
            >
          Home         
           </Button>
            <br />
        </Container>
      )
    }
}

export default SOPs;
