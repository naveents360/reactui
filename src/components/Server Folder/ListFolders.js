import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import {API_URL,getToken} from './../constants'
import { Link, Redirect } from  'react-router-dom';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import DailogOpen from './../DailogOpen';


class ListFolders extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      return:false,
      filename:'',
      ip:'',
      prevfilename:'',
      disabled:false,
      home:false,
      open:false
  }
  }        

  handleClick= filename => event =>{
    event.preventDefault();
    if(this.state.disabled){
      return;
    }    
    let fn = this.state.filename+'/'+filename
    console.log(fn)
    this.updateclick(fn)
    this.setState({filename:fn})
}

handleClickCreateFolderButtton = event =>{
  event.preventDefault();
  console.log(this.state.open)
  this.setState({open:true})  
}

closedailog = () =>{
  window.location.reload();
  console.log(this.state.open)
  this.setState({open:false})  
}


handleClickBackButton = event =>{
    event.preventDefault()
    var pvfn = this.state.filename
    pvfn = pvfn.substring(0,pvfn.lastIndexOf('/'))
    console.log(pvfn)
    if (pvfn === "/home" || pvfn === "/" || pvfn === ""){
        this.setState({isloaded:!this.state.isloaded})
    }
    this.setState({filename:pvfn})
    
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/listdirback?ip='+this.state.ip+'&path='+this.state.filename, { headers: {'Authorization': AuthStr}})
      .then(res => {
        if(res.status === 201 || res.status === 200){
          console.log("Here")
        //alert(res.data.data[0].message);
        console.log(res.data.data)
        this.setState({items:res.data.data})
        //this.setState(previousState => ({filename:previousState.filename}))
      }
            })
    .catch(error => {
      if(error.response){
        //console.log(error.response)
        this.setState({return:!this.state.return})
    }
      })     

}

updateclick(fn){
  this.setState({disabled:!this.state.disabled})
    console.log(this.state.prevfilename)
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/listdir?ip='+this.state.ip+'&path='+fn, { headers: {'Authorization': AuthStr}})
      .then(res => {
        if(res.status === 201 || res.status === 200){
          console.log("Here")
        //alert(res.data.data[0].message);
        console.log(res.data.data)
        this.setState({items:res.data.data})
        this.setState({disabled:!this.state.disabled})
      }
            })
    .catch(error => {
      if(error.response){
        //console.log(error.response)
        this.setState({return:!this.state.return})
    }
      })     
}


    componentDidMount() {
      //
     // window.location.reload(true);
        console.log(this.props.location.state.ip)
      this.setState({ip:this.props.location.state.ip})
        console.log(this.props.location.state.filename)
      this.setState({filename:this.props.location.state.filename})
            let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN); 
      axios.get(API_URL+'/listdir?ip='+this.props.location.state.ip+'&path='+this.props.location.state.filename, { headers: {'Authorization': AuthStr}})
        .then(res => {
          if(res.status === 201 || res.status === 200){
            console.log("Here")
          //alert(res.data.data[0].message);
          console.log(res.data.data)
          this.setState({items:res.data.data})
        }
              })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
  if(this.state.isloaded){
      return <Redirect to={{pathname:"/listfoldersmainpage",  state:{
        ip : this.state.ip                 
      }}}/>
  }
    return (
      
        <TableContainer>
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              IP:- {this.state.ip}  
        </Typography>
        <Divider  fullwidth />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              PWD:- {this.state.filename}  
        </Typography>
        <Divider  fullwidth />
        <br/>
        <Grid container spacing={5} alignItems={'center'}style={{ width: "100%" }}>
        <Grid item xs={1.25}>                
          <Button
          //style="margin:5px"
          type="submit"
            variant="contained"
            color="primary"
            size="medium"
            onClick = {this.handleClickBackButton}
            >
            Back           
           </Button>     
           </Grid>
           <Grid item xs={1.25}>                
          <Button
          //style="margin:5px"
          type="submit"
            variant="contained"
            color="primary"
            size="medium"
            onClick = {this.handleClickCreateFolderButtton}
            >
            Create Folder          
           </Button>     
           </Grid>
           <Grid item xs={1.25}> 
           <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={() =>this.props.history.push('/home')}
            >
          
          Home         
           </Button>
          </Grid> 
           </Grid>
           <br/>

           <Divider  fullwidth />  
          
           <br/>      
            
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
            <TableCell>Permission</TableCell>
              <TableCell>Owner</TableCell>
              <TableCell align="center">Group Owner&nbsp;</TableCell>
              <TableCell align="center">Size</TableCell>
              <TableCell align="center">Month&nbsp;</TableCell>
              <TableCell align="center">Date&nbsp;</TableCell>
              <TableCell align="center">Time&nbsp;</TableCell>
              <TableCell align="center">Filename&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map((item) => (
              <TableRow key={item.filename}>
              <TableCell>{item.permission}</TableCell>
              <TableCell component="th" scope="row">{item.owner}</TableCell>
                <TableCell align="center">{item.group_owner}</TableCell>
                <TableCell align="center">{item.size}</TableCell>
                <TableCell align="center">{item.month}</TableCell>
                <TableCell align="center">{item.date}</TableCell>
                <TableCell align="center">{item.time}</TableCell>
                <TableCell align="center">
                    {item.isdir  ?
                    <Button 
                    style={{textTransform: 'lowercase'}}
                    disabled = {this.state.disabled}
                    onClick={this.handleClick(item.filename)}><Link>{item.filename}</Link></Button> : <div>{item.filename}</div>}
                    </TableCell>                            
              </TableRow>
            ))}
          </TableBody>
        </Table>
                    <div><DailogOpen
                    open = {this.state.open}
                    closedailog = {this.closedailog}
                    title="Create Folder" body="Please Enter Folder Name to be Created" ip={this.state.ip} folderpath={this.state.filename}/></div>
      </TableContainer>
    );
  }
 }

 export default ListFolders;