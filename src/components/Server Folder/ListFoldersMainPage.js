import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import {API_URL,getToken} from './../constants'
import { Link, Redirect } from  'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

class ListFoldersMainPage extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      return:false,
      ip:''
  }
  }        
    componentDidMount() {
      console.log(this.props.location.state.ip)
      this.setState({ip:this.props.location.state.ip})
      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN); 

      axios.get(API_URL+'/listdirmainpage?ip='+this.props.location.state.ip, { headers: {'Authorization': AuthStr}})
        .then(res => {
          if(res.status === 201){
          //alert(res.data.data[0].message);
          console.log(res.data)
          this.setState({items:res.data.data})
        }
              })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
    return (
      
      <TableContainer>
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              IP:- {this.state.ip}  
        </Typography>
        <Divider  fullwidth />
        
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
            <TableCell>Permission</TableCell>
              <TableCell>Owner</TableCell>
              <TableCell align="center">Group Owner&nbsp;</TableCell>
              <TableCell align="center">Size</TableCell>
              <TableCell align="center">Month&nbsp;</TableCell>
              <TableCell align="center">Date&nbsp;</TableCell>
              <TableCell align="center">Time&nbsp;</TableCell>
              <TableCell align="center">Filename&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map((item) => (
              <TableRow key={item.filename}>
              <TableCell>{item.permission}</TableCell>
              <TableCell component="th" scope="row">{item.owner}</TableCell>
                <TableCell align="center">{item.group_owner}</TableCell>
                <TableCell align="center">{item.size}</TableCell>
                <TableCell align="center">{item.month}</TableCell>
                <TableCell align="center">{item.date}</TableCell>
                <TableCell align="center">{item.time}</TableCell>
              <TableCell align="center">
                    <Link to={{pathname:"/listfolders",  state:{
                    filename : item.filename,
                    ip: this.state.ip
                }}}>{item.filename}</Link> 
                    </TableCell>              
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
 }

 export default ListFoldersMainPage;