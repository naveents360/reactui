import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import {API_URL, getToken} from './constants'
import LinearProgress from '@material-ui/core/LinearProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Redirect } from  'react-router-dom';

class UploadFile extends React.Component{
    constructor(props){
        super(props);
        this.state={
            ip:'',
            filelocation:'',
            selectedFile:'',
            uploadPercentage: 0,
            open:false,
            loading:false,
            message:'',
            return:false

        }

    }

    handleClose = event =>{
      event.preventDefault();
      this.setState({open:!this.state.open})
    }

    handleClick = ()=>{
      
        const formData = new FormData();
        formData.append("ip",this.state.ip);
        formData.append("filelocation",this.state.filelocation);
        formData.append("document",this.state.selectedFile)
   

        const options = {
            onUploadProgress: (progressEvent) => {
              const {loaded, total} = progressEvent;
              let percent = Math.floor( (loaded * 100) / total )
             
      
              if( percent < 100 ){
                this.setState({ uploadPercentage: percent })
              }
            }
          }
         
          let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const header = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        this.setState({loading:!this.state.loading})
        this.setState({open: !this.state.open })
        Axios.post(API_URL+'/upload',formData,header,options)
        
        .then(res => {
            this.setState({uploadPercentage: 0})
            this.setState({message:res.data.message})
            this.setState({loading:!this.state.loading})
            //this.setState({open: !this.state.open})
        }).catch(error => {
          if(error.response){
           // console.log(error.response)
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({message:''})
    }

    onFileChange  = event => {
        event.preventDefault();
        //console.log(event.target.files[0])
        this.setState({selectedFile:event.target.files[0]})
       // console.log(this.state.selectedFile.name)
    }

    handleChangefilelocation = event =>{
        event.preventDefault();
        
        this.setState({filelocation : event.target.value})
       // console.log(this.state.filelocation)
    }
    componentDidMount() {
        this.setState({ip : this.props.location.state.ip})
    }

    
    render(){
        const disabled = !this.state.filelocation.length;
        const {uploadPercentage} = this.state;
        if(this.state.return){
          return <Redirect to="/signin"/>
      }
      return(
          
              <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              {this.state.ip}  
        </Typography>
        <TextField           
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Destination Path"
            name="filelocation"      
            autoFocus
            onChange = {this.handleChangefilelocation}
            helperText = {this.state.iphelptext}  
            value={this.state.filelocation}          
          />
          <br/>
        <Typography component="h1" variant="h5" > 
             Select a File to upload
        </Typography>
        <Button
  variant="contained"
  component="label"
>
  Select File
  <input
    type="file"
    style={{ display: "none" }}
    onChange={this.onFileChange}
  />
</Button>
{this.state.selectedFile.name}
<br/>
{ uploadPercentage > 0 && <LinearProgress variant="determinate" value={uploadPercentage} /> }

<br/>
    <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={this.handleClick}
            >
            Submit            
           </Button>     
        
           <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>
         </Container>
      )}}

export default UploadFile;