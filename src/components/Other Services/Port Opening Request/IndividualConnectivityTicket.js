import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken} from './../../constants';
import { Redirect } from  'react-router-dom';


class IndividualConnectivityTicket extends React.Component{
    constructor(props){
        super(props);
        this.state={
        title:'',
        description:'',
        business_justification:'',
        status:'',
        assignee_name:'',
        createdby:'',
        userhprojectelptext:'',
        ticketnumber:'',
        id:'',
        project:''

        }     
       }

       componentDidMount(){
           this.state.id = this.props.location.state.id
        this.setState({id : this.props.location.state.id})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getindividualconnectivityticket?id='+this.state.id, { headers: { 'Authorization': AuthStr } })
 .then(response => {
     // If request is good...
     console.log(response.data[0].id)

     this.setState({title:response.data[0].title})
     this.setState({description:response.data[0].description})
     this.setState({business_justification:response.data[0].business_justification})
     this.setState({project:response.data[0].project})
     this.setState({ticketnumber:response.data[0].ticketnumber})
     this.setState({assignee_name:response.data[0].assignee_name})
     this.setState({status:response.data[0].status})
     this.setState({createdby:response.data[0].createdby})
     this.setState({output:!this.state.output})

  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }        
    render(){
      //const disabled = !this.state.fqdn.length || !this.state.email.length || !this.state.project.length || !this.state.state.length || !this.state.location.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <form  noValidate>
          <TextField           
          disabled = "True"
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Title"
            name="common Name"      
            autoFocus
            value={this.state.title}          
          />
          <br/>
          <TextField
          disabled = "True"
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Description"
            autoFocus
            value = {this.state.description}
          />
          <br />
           <TextField
           disabled = "True"
              variant="outlined"
              margin="normal"
              fullWidth
              label="Ticket Number"
              value = {this.state.ticketnumber}
              />  
              
          <br />
          <TextField
           disabled = "True"
              variant="outlined"
              margin="normal"
              fullWidth
              label="Requested By"
              autoFocus
              value = {this.state.createdby}
              />                
          <br />
      <TextField
      disabled = "True"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Business Justifiaction"
              value = {this.state.business_justification}
              />
           <br/>
           <TextField
              variant="outlined"
              disabled = "True"
              margin="normal"
              required
              fullWidth
              name="state"
              label="Status"
              value = {this.state.status}
              />
           <br/>
           <TextField
           disabled = "True"
              variant="outlined"
              margin="normal"
              fullWidth
              label="Assignee Name"
              value = {this.state.assignee_name}
              />  
              <br />
              <TextField
           disabled = "True"
              variant="outlined"
              margin="normal"
              fullWidth
              label="Project"
              value = {this.state.project}
              />  
              <br />


        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default IndividualConnectivityTicket;