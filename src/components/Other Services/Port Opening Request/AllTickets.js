import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import {API_URL,getToken} from './../../constants'
import { Link, Redirect } from  'react-router-dom';
import Button from '@material-ui/core/Button';

class AllTickets extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      isadmin:false,
      return:false,
  }
  }
  
  downloadCsr = parameter =>{
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.get(API_URL+'/downloadexcelfile?id='+parameter,{ headers: {'Authorization': AuthStr},responseType: 'arraybuffer'})
            .then(response => {
              if(response.status === 200){
              //alert(res.data.data[0].message);
              var data = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
              const url = window.URL.createObjectURL(data);
              console.log(url)
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download",'Generic Template.xlsx');
              document.body.appendChild(link);
              link.click();
              link.remove();
            }
            if(response.status === 201){
              this.setState({message:response.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              //console.log(error.response)
              this.setState({return:!this.state.return})
          }
            })     
  }
    componentDidMount() {
      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN); 
      axios.get(API_URL+'/getownership', { headers: {'Authorization': AuthStr}})      
      .then(response => {
         let ownership = response.data.ownership;
         
          if(ownership === "admin"){
              this.setState({isadmin:!this.state.isadmin})
          }
          if(ownership === "member"){
             this.setState({ismember:!this.state.ismember})
         }
        
       })
      .catch((error) => {
         if(error.response){
             this.setState({return:!this.state.return})
         }
       });

      axios.get(API_URL+'/getportopeningrequest', { headers: {'Authorization': AuthStr}})
        .then(res => {
          if(res.status === 200){
          //alert(res.data.data[0].message);
          this.setState({items:res.data.tickets})
          let sortedField="project";
          let sorteditems = [...res.data.data];
          sorteditems.sort((a,b)=>{
             if (a[sortedField] < b[sortedField]) {
            return -1;
          }
          if (a[sortedField] > b[sortedField]) {
            return 1;
          }
          return 0;
        });
        this.setState({items:sorteditems})
        }
              })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
  return (
      
    <TableContainer>
      <CssBaseline />
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
          <TableCell>Sl.No</TableCell>
            <TableCell>Ticket Number</TableCell>
            <TableCell align="center">Status</TableCell>
            {/* <TableCell align="center">Password&nbsp;</TableCell> */}
            <TableCell align="center">Project&nbsp;</TableCell>
            <TableCell align="center">View details&nbsp;</TableCell>
            <TableCell align="center">FCR FORM</TableCell>
            { this.state.isadmin && <TableCell align="center">
            Update Details</TableCell>}
            {/* <TableCell align="center">Delete&nbsp;</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {items
          .map((item,index) => (
            <TableRow key={item.id}>
            <TableCell>{index+1}</TableCell>
              <TableCell component="th" scope="row">
                {item.ticketnumber}
              </TableCell>
              <TableCell align="center">{item.status}</TableCell>
              {/* <TableCell align="center">{item.password}</TableCell> */}
              <TableCell align="center">{item.project}</TableCell>      
              <TableCell align="center">
              <Link to={{pathname:"/individualconnectivityticket",  state:{
                  id  : item.id,
              }}}>View Details</Link>           
            </TableCell>     
            <TableCell align="center">
            <Button
            
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={() => this.downloadCsr(item.id)}
            >
            Download          
           </Button> 
           </TableCell>
           <TableCell align="center">
           {this.state.isadmin && <Button><Link to={{pathname:"/updateconnectivityticket",  state:{
                  id  : item.id,
              }}}>Update Details</Link></Button>}
           </TableCell>
            {/* <TableCell align="center">
            <DeleteIcon fontSize="small" onClick={this.handleDailog(item.ip)}/>
            </TableCell> */}
  
            
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    
  );
  }
 }

 export default AllTickets;