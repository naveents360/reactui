import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken,encryptdata} from './../../constants';
import { Redirect } from  'react-router-dom';
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';


class RaiseNewRequest extends React.Component{
    constructor(props){
        super(props);
        this.state={
        title:'',
        description:'',
        business_justifiaction:'',
        message:'',
        project:'',
        uploadPercentage: 0,
        open:false,
        loading:false,
        projects:[],
        return:false,
        redirect:false,
        selectedFile:'',
        }     
       }
       componentDidMount(){
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN);         
        axios.get(API_URL+'/getprojectlist', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     // If request is good...
     this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }
       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
         //this.setState({redirect:!this.state.redirect})
       }
       handleChangebusiness_justification = event =>{
        event.preventDefault();
        this.setState({business_justifiaction: event.target.value})
    }

    handleProject = event =>{
        event.preventDefault();
        this.setState({project:event.target.value})
    }
       handleChangedescription = event =>{
        event.preventDefault();
        
        this.setState({description: event.target.value})
    }

       handleChangetitle = event =>{
           event.preventDefault();
           this.setState({title: event.target.value})           
           }

       onFileChange  = event => {
        event.preventDefault();
        //console.log(event.target.files[0])
        this.setState({selectedFile:event.target.files[0]})
       // console.log(this.state.selectedFile.name)
    }

        handleClick(event){
        event.preventDefault();
        console.log(this.state.description)
        //this.setState({open: !this.state.open })
        //this.setState({loading: !this.state.loading})
        const formData = new FormData();
        formData.append("title",this.state.title);
        formData.append("description",this.state.description);
        formData.append("business_justification",this.state.business_justifiaction);
        formData.append("project",this.state.project)
        formData.append("assignee_name","None")
        formData.append("ticketnumber","-")
        formData.append("status","ticket not raised")
        formData.append("firewallform",this.state.selectedFile)   

       let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const header = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        this.setState({loading:!this.state.loading})
        this.setState({open: !this.state.open })
        axios.post(API_URL+'/raiseportopeningrequest', formData,header)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
                //console.log(res.data)
            this.setState({message:res.data.msg})           
          }
          else if(res.status === 201){
             
              this.setState({message:res.data.msg})
          }
        })
        .catch(error => {
          if(error.response){            
            this.setState({return:!this.state.return})
        }
          })     
       //   this.setState({username:'',Port:'',password:'',serverip:'',remoteserverip:'',userhelptext:'',iphelptext:'',remoteiphelptext:'',passhelpertext:'',env:'',project:''})
        }
        
        

         
        
    render(){
      //const disabled = !this.state.serverip.length || !this.state.env.length || !this.state.password.length || !this.state.project.length || !this.state.username.length;
      const disabled = false
      if(this.state.return){
        return <Redirect to="/signin"/>
      
    }
    if(this.state.redirect){
      return <Redirect to="/alltickets"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               New Connectivity Request
        </Typography>
        <form  noValidate>
          <TextField           
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            multiline
            label="Title"
            name="title"      
            autoFocus
            onChange = {this.handleChangetitle}
            helperText = {this.state.iphelptext}  
            value={this.state.title}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            multiline
            label="Description"
            name="description"     
            onChange = {this.handleChangedescription}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.description}
          />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              multiline
              name="business_justification"
              label="Business Justification"
              //type="password"
              onChange = {this.handleChangebusiness_justification}
              value = {this.state.business_justifiaction}
              helperText = {this.state.passhelpertext}
              />
           <br/>

    <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Project</InputLabel>
        <Select          
          value={this.state.project}
          onChange={this.handleProject}
          label="Project"
        >
          {
            this.state.projects.map((project,index)=>
            <MenuItem key={index} value={project}>{project}</MenuItem>)
          }
        </Select>
      </FormControl>
      <Typography component="h1" variant="h5" > 
            Please Attach FCR Form
        </Typography>
        <Button
  variant="contained"
  component="label"
>
  Select File
  <input
    type="file"
    style={{ display: "none" }}
    onChange={this.onFileChange}
  />
</Button>
{this.state.selectedFile.name}
<br/>
<br/>
      
          
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Submit            
           </Button>                
            <br/>        

            
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default RaiseNewRequest;