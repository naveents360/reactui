import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken} from './../../constants';
import { Redirect } from  'react-router-dom';
//import CircularProgress from '@material-ui/core/CircularProgress';



class UpdateconnectivityRequest extends React.Component{
    constructor(props){
        super(props);
        this.state={
        status:'',
        assignee_name:'',
        message:'',
        ticketnumber:'',
        open:false,
        loading:false,
        return:false,
        }     
       }
       handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }

       componentDidMount() {
        //console.log(this.props.location.state)
        this.state.id = this.props.location.state.id
        this.setState({id : this.props.location.state.id})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getindividualconnectivityticket?id='+this.props.location.state.id, { headers: { 'Authorization': AuthStr } })
 .then(response => {
     // If request is good...
     this.setState({ticketnumber:response.data[0].ticketnumber})
     this.setState({assignee_name:response.data[0].assignee_name})
     this.setState({status:response.data[0].status})
     this.setState({output:!this.state.output})

  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }


       handleChangeAssigneeName = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({assignee_name: event.target.value})
    }

       handleChangeStatus = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({status: event.target.value})
    }
    
       handlechangeticketnumber= event =>{
           event.preventDefault();
           this.setState({ticketnumber:event.target.value})
       }

        handleClick(event){
        event.preventDefault();
            const payload = {
               "ticketnumber":this.state.ticketnumber,
               "status":this.state.status,
               "assignee_name":this.state.assignee_name
           } 
  
          this.setState({open: !this.state.open })
          this.setState({loading: !this.state.loading})  
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        //console.log(payload)
        //console.log(options)
        axios.put(API_URL+'/updateportopeningform?id='+this.state.id, payload,options)
        .then(res => {
          this.setState({loading: !this.state.loading})
            if(res.status === 200){
            //console.log(res.data.message)
            //alert(res.data.data[0].message);
            this.setState({message:res.data.msg})
            //console.log(this.state.message);
            //console.log("Login successfull");
          }
          else if(res.status === 201){
              //console.log(res.data.message);
              this.setState({message:res.data.msg})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
          //this.setState({username:'',Port:'',password:'',serverip:'',remoteserverip:'',userhelptext:'',iphelptext:'',remoteiphelptext:'',passhelpertext:'',env:'',project:''})
        }  
        
    render(){
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Update Credentials
        </Typography>
        <form  noValidate>
          <TextField            
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Ticket Number"
            name="ticket number"      
            autoFocus
            onChange = {this.handlechangeticketnumber}
            value={this.state.ticketnumber}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Assignee Name"
            name="assignee_name"     
            onChange = {this.handleChangeAssigneeName}
            autoFocus
            value = {this.state.assignee_name}
          />
           <br/>
      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Status</InputLabel>
        <Select          
          value={this.state.status}
          onChange={this.handleChangeStatus}
          label="Environment"
        >
          <MenuItem value="ticket not raised">Ticket Not Raised</MenuItem>
          <MenuItem value="pending">Pending</MenuItem>
          <MenuItem value="workinprogress">Work In Progress</MenuItem>  
          <MenuItem value="closed">Closed</MenuItem>  
        </Select>
      </FormControl>          
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Update            
           </Button>
            <br/>            
            <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default UpdateconnectivityRequest;