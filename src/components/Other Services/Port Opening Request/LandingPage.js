import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';
import axios from 'axios';
import {API_URL,getToken,verifyheader} from './../../constants'

class LandingPage extends React.Component{

    handlegenericfiledownload = () =>{
        //console.log(parameter)
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN);
        axios.get(API_URL+'/downloadgenerictemplates/fcrform',{ headers: {'Authorization': AuthStr},responseType: 'arraybuffer'})
                .then(response => {
                  if(response.status === 200){
                  //alert(res.data.data[0].message);
                  var data = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
                  const url = window.URL.createObjectURL(data);
                  console.log(url)
                  const link = document.createElement("a");
                  link.href = url;
                  link.setAttribute("download",'Generic Template.xlsx');
                  document.body.appendChild(link);
                  link.click();
                  link.remove();
                }
                if(response.status === 201){
                  this.setState({message:response.data.message})
                }
    
                      })
              .catch(error => {
                if(error.response){
                  //console.log(error.response)
                  this.setState({return:!this.state.return})
              }
                })     
            
      }
      componentDidMount(){
        verifyheader()
      }
   
    render(){
      
      return(
        <Container style={{textAlign:"center"}}>
          <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Port Connectivity Request
            </Typography>
            <Link  to={{pathname:"/raisenewconnectivityrequest"}}>
          <Button
          //style="margin:5px"
          type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            New Request           
           </Button>   
           </Link>   
            <br />
            <br />
            <Link  to={{pathname:"/alltickets"}}>
          <Button
          //style="margin:5px"
          type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            Available Requests          
           </Button>   
           </Link>  
            <br />
            <br />
            
          <Button
          //style="margin:5px"
          type="submit"
            variant="contained"
            color="primary"
            size="medium"
            onClick={() => this.handlegenericfiledownload()}
            >
            Download FCR Form         
           </Button>   
           
        </Container>
      )
    }
}

export default LandingPage;
