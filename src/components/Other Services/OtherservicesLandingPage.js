import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';
import {API_URL,getToken} from '../constants'
import axios from "axios";
import {Redirect} from 'react-router-dom'

class OtherServicesLandingPage extends React.Component{
  constructor(props){
    super(props);
    this.state={
    return:'',
  }
}

  componentDidMount(){
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/testdecodejwt', { headers: {'Authorization': AuthStr}})      
    .then(response => {
        return response.status
     })
    .catch((error) => {
        if(error.response){
       this.setState({return:!this.state.return})
        }
     });
  }
    render(){
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
      
      return(
        <Container style={{textAlign:"center"}}>
          <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Other Services
            </Typography>
            <Button><Link to="/Portopeninglandingpage">Port Connectivity Request</Link></Button>
            <br />
        </Container>
      )
    }
}

export default OtherServicesLandingPage;
