import { Link,Redirect } from  'react-router-dom';
import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {API_URL,getToken} from './constants';
import axios from 'axios';



class ProjectLinks extends React.Component{
    constructor(props){
        super(props);
        this.state={
            projects:[],
            return:false
        }
       }

    componentDidMount(){
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getprojectlist', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
    if(error.response){
        if(error.response){
            this.setState({return:!this.state.return})
        }
     }
  });
    }
    render(){
        if(this.state.return){
            return <Redirect to="/signin"/>
        }
      return(
        <Container component="main" maxWidth="xs"style={{textAlign:"center"}}>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Project List
            </Typography>
            {
            this.state.projects.map((project,index)=>
            <div key={index}>
                <Button>
            <Link to={{pathname:"/projectdata",  state:{
                projectname : {project},
            }}}>{project}</Link></Button><br/></div> 
            )
          }
         </Container>
   )
    }
  }
    
export default ProjectLinks;