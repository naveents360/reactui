
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {API_URL, getToken} from './constants';
import { Redirect } from  'react-router-dom';

class ExecuteCommand extends React.Component{
    constructor(props){
        super(props);
        this.state={
        ip:'',
        command:'',
        items:[],
        return:false

        }     
       }

       componentDidMount() {
        this.setState({ip : this.props.location.state.ip})
       }

       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }
      handleChangecommand = event =>{
           event.preventDefault();
           this.setState({command: event.target.value})
       }         
        handleClick(event){
        event.preventDefault();
        this.setState({open: !this.state.open })
        this.setState({loading: !this.state.loading})
            const payload = {
               "ip":this.state.ip,
               "command":this.state.command
           }         
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        axios.post(API_URL+'/executecommand', payload,options)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
            this.setState({items:res.data.data})         
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
        }         
        
    render(){
      const disabled = !this.state.command.length;
      var {items}=this.state;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Execute Command
        </Typography>
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               {this.state.ip}  
        </Typography>
        <form  noValidate>
          <TextField           
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="command"
            name="command"      
            autoFocus
            onChange = {this.handleChangecommand}
            helperText = {this.state.iphelptext}  
            value={this.state.command}          
          /><br/>
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Execute            
           </Button>       
           <br/>
           {items && items.map((data,index)=>
               <div key={index}>
               <p>{data.output}</p></div>
               )
           }
          {/* {
              this.state.message.map((i,index)=>
              <p key={index}>{i} </p>)
          } */}
          

            <br/>            
        {/* <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading }</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {items && items.map((data)=>
               <div>
               <p>{data.output}</p></div>
               )
           }
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog> */}

        </form>
        </Container>
    )

}
}

export default ExecuteCommand;