import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import axios from 'axios'
import {API_URL,getToken} from './constants'

class DailogOpen extends React.Component{
    constructor(props){
        super(props);
        this.state={
        foldername:'',
        foldernamehelptext:''
        }
    }
     
    handleClose = event  =>{
        event.preventDefault();
        console.log(this.state.foldername)
        if(!this.state.foldername.match(/^[0-9a-zA-Z_]+$/)){
           this.setState({foldernamehelptext:"Only AlphaNumeric Values Allowed"}) 
          return        
        }
         console.log(this.props.filename)
        let payload = {
          "ip":this.props.ip,
          "command": "mkdir -p "+this.props.folderpath+'/'+this.state.foldername
      }    

   console.log(payload)         
   this.setState({message:''})
   let TOKEN = getToken();
   const AuthStr = 'Bearer '.concat(TOKEN); 
   const options = {
     headers: {
         'Authorization': AuthStr,
     }
   };
   axios.post(API_URL+'/executecommand', payload,options)
   .then(res => {
     this.setState({loading:!this.state.loading})
       if(res.status === 200){
        console.log(res.data)
       this.setState({items:res.data.data})  
           

     }
     else if(res.status === 201){
         this.setState({message:res.data.message})
     }
     this.setState({foldername:''})
     this.props.closedailog();
   })
   .catch(error => {
     if(error.response){
       this.setState({return:!this.state.return})
   }
     })             
        this.setState({open: !this.state.open })
    }
    
    

    render(){
      return(
        <div>
         <Dialog          
          open={this.props.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{this.props.title}</DialogTitle>
          <DialogContent>
            
            <DialogContentText>
            {this.props.body}
            </DialogContentText>
            <TextField           
        //variant="outlined"
        margin="dense"
        fullWidth
        required
        label="Folder Name"
        name="Folder Name"      
        autoFocus
        helperText = {this.state.foldernamehelptext}  
        onChange = {(event)=> this.setState({foldername:event.target.value})}
        value={this.state.foldername}          
      />
          </DialogContent>
          <DialogActions>
         <Button onClick={this.props.closedailog} color="primary">
             No
          </Button>
          <Button
           onClick={this.handleClose} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>
      </div>
            )
    }
  }
    
export default DailogOpen;