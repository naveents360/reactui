import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import {API_URL,getToken} from './../constants'
import { Redirect } from  'react-router-dom';

class Inventory extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      return:false,
  }
  }        
    componentDidMount() {
      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN); 

      axios.get(API_URL+'/getinv', { headers: {'Authorization': AuthStr}})
        .then(res => {
          if(res.status === 200){
          //alert(res.data.data[0].message);
          this.setState({items:res.data.msg})
          //console.log(this.state.items)
        //   let sortedField="App Name";
        //   let sorteditems = [...res.data.data];
        //   sorteditems.sort((a,b)=>{
        //      if (a[sortedField] < b[sortedField]) {
        //     return -1;
        //   }
        //   if (a[sortedField] > b[sortedField]) {
        //     return 1;
        //   }
        //   return 0;
        // });
        // this.setState({items:sorteditems})
        }
              })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
    return (
      
      <TableContainer>
        <CssBaseline />
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
            <TableCell>Sl.No</TableCell>    
            <TableCell>App Name</TableCell>
              <TableCell>ENV</TableCell>
              <TableCell align="center">Location&nbsp;</TableCell>
              <TableCell align="center">VM Type</TableCell>
              <TableCell align="center">Server Type&nbsp;(Avail)</TableCell>
              <TableCell align="center">serialNumber&nbsp;(Avail)</TableCell>
              <TableCell align="center">OS&nbsp;</TableCell>
              <TableCell align="center">OS Version&nbsp;</TableCell>
              <TableCell align="center">Hostname&nbsp;</TableCell>
              <TableCell align="center">IP&nbsp;</TableCell>
              <TableCell align="center">Component&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map((item,index) => (
              <TableRow key={item.ip}>
              <TableCell>{index+1}</TableCell>
              <TableCell component="th" scope="row">{item.AppName}</TableCell>
                <TableCell align="center">{item.ENV}</TableCell>                
                <TableCell align="center">{item.Location}</TableCell>
                <TableCell align="center">{item.VMType }</TableCell>
                <TableCell align="center">{item.ServerType}</TableCell>
                <TableCell align="center">{item.serialNumber}</TableCell>
                <TableCell align="center">{item.OS}</TableCell>
                <TableCell align="center">{item.OSVersion}</TableCell>
                <TableCell align="center">{item.Hostname}</TableCell>
                <TableCell align="center">{item.IP}</TableCell>
                <TableCell align="center">{item.Component}</TableCell>
                
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
 }

 export default Inventory;