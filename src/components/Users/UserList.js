import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import { API_URL,getToken,encryptstring } from "./../constants";
import {  Redirect } from  'react-router-dom';
import Typography from '@material-ui/core/Typography';
//import TrashIcon from '@material-ui/svg-icons/action/delete'

class UserList extends React.Component{
    constructor(props){
        super(props);
        this.state={
          items:  [],
          isloaded: false,
          projectname:'',
          message:'',
          open:false,
          username:'',
          isdeleted:false,
          ismember:true
      }
      }

      handleDelete = (event) =>{
        event.preventDefault();
        this.setState({open:!this.state.open}) 
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN);  
        axios.delete(API_URL+"/deleteuserfromproject?username="+encryptstring(this.state.username)+"&project="+encryptstring(this.state.projectname.project),{headers: {'Authorization': AuthStr}})
        .then(res => {
          window.location.reload(true);
            if(res.status === 200){
            this.setState({isdeleted:!this.state.isdeleted})
            this.setState({message:res.data.data[0].message})
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
      }

      handleDailog = parameter => event =>{
        event.preventDefault();
        this.setState({username:parameter})
        this.setState({open:!this.state.open}) 
      }
      handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})  
      }
            
        componentDidMount() {
            this.state.projectname=this.props.location.state.projectname
            let TOKEN = getToken();
            const AuthStr = 'Bearer '.concat(TOKEN); 

            axios.get(API_URL+'/getownership', { headers: {'Authorization': AuthStr}})      
            .then(response => {
               let ownership = response.data.ownership;
                if(ownership === "admin"){
                    this.setState({isadmin:!this.state.isadmin})
                }
                if(ownership === "member"){
                   this.setState({ismember:!this.state.ismember})
               }
              
             })
            .catch((error) => {
               if(error.response){
                   this.setState({return:!this.state.return})
               }
             });
            axios.get(API_URL+'/users/'+this.state.projectname.project,{ headers: {'Authorization': AuthStr}})
            .then(res => {
              if(res.status === 200){
              let sortedField="role";
              let sorteditems = [...res.data.message.users];
              sorteditems.sort((a,b)=>{ if (a[sortedField] < b[sortedField]) {
                return 1;
              }
              if (a[sortedField] > b[sortedField]) {
                return -1;
              }
              return 0;
            });
            this.setState({items:sorteditems})
            }
            if(res.status === 201){
              this.setState({message:res.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              this.setState({return:!this.state.return})
          }
            })     
        
        }
      render() {
        var {items}=this.state;
        if(this.state.return){
          return <Redirect to="/signin"/>
      }
        return (
          
          <TableContainer>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               {this.state.projectname.project}
        </Typography>
            <Table aria-label="customized table">
            <TableHead>
            <TableRow>
            <TableCell>Sl.No</TableCell>
              {/* <TableCell>IP</TableCell> */}
              <TableCell align="center">User Name</TableCell>
              {/* <TableCell align="center">Password&nbsp;</TableCell> */}
              <TableCell align="center">Role&nbsp;</TableCell>
              {/* <TableCell align="center">Project&nbsp;</TableCell> */}
        {this.state.ismember && <TableCell align="center">Delete</TableCell> }
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map((item,index) => (
              <TableRow key={index}>
                <TableCell>{index+1}</TableCell>
                {/* <TableCell component="th" scope="row">
                {item.ip}                 
                </TableCell> */}
                <TableCell align="center">{item.username}</TableCell>
                {/* <TableCell align="center">{item.password}</TableCell> */}
                <TableCell align="center">{item.role}</TableCell>
                {/* <TableCell align="center">{item.projectname}</TableCell>        */}
                {/* <TableCell align="center">
                <Link to={{pathname:"/individualservercredentials",  state:{
                    ip : item.ip,
                    username : item.username,
                    password:item.password,
                    env:item.env,
                    project:item.project
                }}}>Edit</Link>           
              </TableCell>         */}
            {this.state.ismember &&  <TableCell align="center">
                {/* <DailogOpen ip={item.ip} /> */}
              <DeleteIcon fontSize="small" onClick={this.handleDailog(item.username)}/>
              </TableCell>}

              <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Status"}</DialogTitle>
          <DialogContent>
            
            <DialogContentText>
            Do You Want To Delete {this.state.username} from {this.state.projectname.project}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         <Button onClick={this.handleClose} color="primary">
             No
          </Button>
          <Button onClick={this.handleDelete} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>
              </TableRow>
              
            ))}
          </TableBody>
            </Table>
          {this.state.isdeleted && <Alert severity="success">Server Deleted SuccessFully</Alert> }
          </TableContainer>
        );
      }
     }
    
export default UserList;