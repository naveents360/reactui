import React, { Component } from 'react';
import { EditorState, Editor,ContentState,convertFromRaw,convertToRaw} from 'draft-js';
import axios from 'axios';
import {API_URL} from '../constants'



class EditorFile extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      editorState:EditorState.createEmpty(), 
      rawdata:'',
      ip:''
    }
  }
  //var data = "[{"key":"1iptu","text":"Hello","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"6q73j","text":"","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"1qn30","text":"every one","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}";

  
  handleSubmit = event =>{
    
    event.preventDefault();   
   //console.log(this.state.editorState.getCurrentContent().getPlainText());
}
  componentDidMount(){
   //this.setState({ip : this.props.location.state.ip})
    const data = {
      "ip":"10.159.16.134",
      "filename":"/etc/yum.conf"
}
//console.log(data)
axios.post(API_URL+'/fileread',data)
        .then(res => {
          const outputdata = res.data

          //console.log(typeof outputdata)
          let jsondata = String(outputdata)

          const dt = EditorState.createWithContent(convertFromRaw(JSON.parse(jsondata)))
      
          //this.setState({editorState:dt});
        })
        .catch(error => {
        
        })     
   // const data = `{"blocks":[{"key":"","text":"Hi Naveen","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"","text":"","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"","text":"Lets go to school","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}`;
   //const data = 


  //   const dt = EditorState.createWithContent(convertFromRaw(JSON.parse(data)))

  //   this.setState({editorState:dt});
    //this.setState({editorState:convertFromRaw(JSON.parse(data))})
   
  }      

  onChange = (editorState) => {
    this.setState({
      editorState,
    });
    const rawDraftContentState = JSON.stringify( convertToRaw(this.state.editorState.getCurrentContent()) );
    this.setState({rawdata:rawDraftContentState})
    //console.log(rawDraftContentState)
  }

  render() {
    return (
      <div>
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}
          spellCheck = {true}
        />
         <button onClick={this.handleSubmit} className="btn-large waves-effect waves-light xbutton">Save</button>
         {this.state.rawdata}
      </div>
      
        
      
    );
  }
}

export default EditorFile;