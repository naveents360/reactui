import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken,encryptdata,validateipaddress,validateusernamelength,validatepassword,verifyheader} from './constants';
import { Redirect } from  'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';


class Onboard extends React.Component{
    constructor(props){
        super(props);
        this.state={
        serverip:'',
        username:'',
        password:'',
        message:'',
        userhelptext:'',
        iphelptext:'',
        project:'',
        env:'',
        open:false,
        loading:false,
        projects:[],
        return:false,
        showPassword:false
        }     
       }

        handleMouseDownPassword = event => {
        event.preventDefault();
        this.setState({showPassword:!this.state.showPassword})
      };

       componentDidMount(){
         //verifyheader()
        //console.log(code)
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getprojectlist', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     // If request is good...
     this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }
       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }
       handleEnv = event =>{
        event.preventDefault();
        this.setState({env: event.target.value})
    }

       handleProject = event =>{
        event.preventDefault();
        
        this.setState({project: event.target.value})
    }

       handleChangeserverip = event =>{
           event.preventDefault();
           this.setState({serverip: event.target.value})
       }
       handleChangeusername= event =>{
           event.preventDefault();
           this.setState({username:event.target.value})
       }
       handleChangepassword = event=>{          
           event.preventDefault();
           this.setState({password:event.target.value})
           }

       validate = ()=>{
        var ip = this.state.serverip;
        var port = this.state.Port
        let passhelpertext = validatepassword(this.state.password)
        let iphelptext = validateipaddress(ip)
        let userhelptext = validateusernamelength(this.state.username)  
        //let porthelptext = validateportnumber(port) 
          if(userhelptext || iphelptext ||  passhelpertext ){
            //console.log(this.state.passhelpertext)
            this.setState({userhelptext,iphelptext,passhelpertext});
            //console.log(this.state.passhelpertext)
             return false;
         }
         return true;

       }
      
        handleClick(event){
        event.preventDefault();
        //this.setState({open: !this.state.open })
        //this.setState({loading: !this.state.loading})
            const payload = {
               "username":this.state.username,
               "password":this.state.password,
               "ip":this.state.serverip,
               "project":this.state.project,
               "env":this.state.env
           } 
           console.log(payload)
          
         const isvalid = this.validate();
       // const isvalid = true
       console.log(isvalid)
        if(isvalid){        
          this.setState({loading: !this.state.loading})
        this.setState({open: !this.state.open})
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        axios.post(API_URL+'/onboardserver', payload,options)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.message})           
          }
          else if(res.status === 201){
             
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){            
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({username:'',Port:'',password:'',serverip:'',remoteserverip:'',userhelptext:'',iphelptext:'',remoteiphelptext:'',passhelpertext:'',env:'',project:''})
        }
        
         }

         
        
    render(){
      const disabled = !this.state.serverip.length || !this.state.env.length || !this.state.password.length || !this.state.project.length || !this.state.username.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Onboard Server
        </Typography>
        <form  noValidate>
          <TextField           
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Server IP Address"
            name="serverip"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.serverip}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Username"
            name="username"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.username}
          />
          <br/>
            {/* <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              onChange = {this.handleChangepassword}
              value = {this.state.password}
              helperText = {this.state.passhelpertext}
              />
           <br/> */}
           <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={this.state.showPassword ? 'text' : 'password'}
            value={this.state.password}
            onChange={this.handleChangepassword}
            fullWidth
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick= {this.handleMouseDownPassword}
                  //onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
          <FormHelperText id="filled-weight-helper-text">{this.state.passhelpertext}</FormHelperText>
        </FormControl>

    <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Project</InputLabel>
        <Select          
          value={this.state.project}
          onChange={this.handleProject}
          label="Project"
        >
          {
            this.state.projects.map((project,index)=>
            <MenuItem key={index} value={project}>{project}</MenuItem>)
          }
        </Select>
      </FormControl>
      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Environment</InputLabel>
        <Select          
          value={this.state.env}
          onChange={this.handleEnv}
          label="Environment"
        >
          <MenuItem value="DEV">DEV</MenuItem>
          <MenuItem value="SIT">SIT</MenuItem>
          <MenuItem value="ST">ST</MenuItem>
          {/* <MenuItem value="REPLICA">REPLICA</MenuItem>     */}
        </Select>
      </FormControl>
      
          
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Submit            
           </Button>                
            <br/>        

            
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default Onboard;