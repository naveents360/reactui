import React from 'react';  
import TablePagination from '@material-ui/core/TablePagination';  

export default function Pagination(props){  
  const [page, setPage] = React.useState(0);  
    const [data, setData] = useState([]);   
    const [rowsPerPage, setRowsPerPage] = React.useState(5);  
    const handleChangePage = (event, newPage) => {  
            setPage(newPage);  
          };  

    const handleChangeRowsPerPage = event => {  
            setRowsPerPage(+event.target.value);  
                            setPage(0);  
              };  

        return(
            <TablePagination  
                    rowsPerPageOptions={[10, 15, 20]}  
                    component="div"  
                    count={data.length}  
                    rowsPerPage={rowsPerPage}  
                    page={page}  
                    onChangePage={handleChangePage}  
                    onChangeRowsPerPage={handleChangeRowsPerPage}  
                  />  
        )
    }
