import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {API_URL,encryptstring} from './constants'
//import {askForPermissioToReceiveNotifications} from './../firebase'

class SignIn extends React.Component{
  constructor(props){
    super(props);
    this.state={
    domainid:'',
    password:'',
    message:''
    }
   }
 

   componentDidMount(){
    encryptstring("Naveen.Ts")
   }

   handleChangedomainid = event=>{
     event.preventDefault();
     this.setState({domainid:event.target.value})
   }

   handleChangepassword = event=>{          
    event.preventDefault();
    this.setState({password:event.target.value})
    }

   validate = ()=>{
       let iphelptext = "";
       let remoteiphelptext = "";

       const ip = this.state.serverip;
       const remip = this.state.remoteserverip;
    const expression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
        if (!expression.test(remip)){
            remoteiphelptext="Invalid IP";
        }
      if(!expression.test(ip)){
        iphelptext = "Invalid IP";
      }
        if( iphelptext || remoteiphelptext ){
           this.setState({iphelptext,remoteiphelptext});
           return false;
       }
       return true;

   }
   
    handleClick(event){
    event.preventDefault();
        const payload = {
           "username":this.state.domainid,
           "password":this.state.password
       } 
    //console.log(payload)
    //const isvalid = this.validate();
    this.setState({message:''})    
    //axios.post(API_URL+'/login',payload)
    axios.get(API_URL+'/login?username='+encryptstring(this.state.domainid)+"&password="+encryptstring(this.state.password))
    .then(res => {
        if(res.status === 200){
            sessionStorage.setItem('accesstoken',res.data.accesstoken)
            sessionStorage.setItem('isloggedin',true)
        this.props.history.push('/home')        
        //this.setState({loading:!this.state.loading})
        this.setState({message:res.data.data[0].message})
      }
      else if(res.status === 201){
          this.setState({message:res.data.message})
      }
    })
    .catch(error => {
        if(error.response){
            this.setState({return:!this.state.return})
        }
      })     
    }
    
     
render(){
    const disabled = !this.state.domainid.length || !this.state.password.length;
return(
    <Container component="main" maxWidth="xs">
    <CssBaseline />
    <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
           Sign In
    </Typography>
    <form  noValidate>
      <TextField           
        variant="outlined"
        margin="normal"
        fullWidth
        required
        label="Domain ID"
        name="Domain ID"      
        autoFocus
        onChange = {this.handleChangedomainid}
        value={this.state.domainid}          
      />
        <br/>
        <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            onChange = {this.handleChangepassword}
            value = {this.state.password}
            helperText = {this.state.passhelpertext}
            />
      <br/>       
      <Button
        disabled={disabled}
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        onClick={(event) => this.handleClick(event)}            
        >
        Login            
       </Button>
    </form>
    <p>{this.state.message}</p>
    </Container>
)
}
}

export default SignIn;