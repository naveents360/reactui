import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import {API_URL,getToken} from './constants'
import { Link, Redirect } from  'react-router-dom';
import Pagination from "react-js-pagination";

class DataTable extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      return:false,
      activePage:10
  }

  } 
  handlePageChange = pageNumber => {
    this.setState({activePage: pageNumber});
  }

    componentDidMount() {
      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN); 

      axios.get(API_URL+'/getdata', { headers: {'Authorization': AuthStr}})
        .then(res => {
          if(res.status === 200){
          //alert(res.data.data[0].message);
          this.setState({items:res.data})
          let sortedField="project";
          let sorteditems = [...res.data];
          sorteditems.sort((a,b)=>{
             if (a[sortedField] < b[sortedField]) {
            return -1;
          }
          if (a[sortedField] > b[sortedField]) {
            return 1;
          }
          return 0;
        });
        this.setState({items:sorteditems})
        }
              })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
    return (
      
      <TableContainer>
        <CssBaseline />
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
            <TableCell>Sl.No</TableCell>
              <TableCell>IP</TableCell>
              <TableCell align="center">Hostname&nbsp;</TableCell>
              <TableCell align="center">Used Space</TableCell>
              <TableCell align="center">RAM&nbsp;(Avail)</TableCell>
              <TableCell align="center">SWAP&nbsp;(Avail)</TableCell>
              <TableCell align="center">OS&nbsp;</TableCell>
              <TableCell align="center">OS Version&nbsp;</TableCell>
              <TableCell align="center">Project&nbsp;</TableCell>
              <TableCell align="center">Env&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map((item,index) => (
              <TableRow key={item.ip}>
              <TableCell>{index+1}</TableCell>
              <TableCell component="th" scope="row">
              {item.passwdauth === "True" ? 
<Link to={{pathname:"/mountpoints/"+item.ip,  state:{
ip : item.ip,
ram : item.ram,
usedspace : item.usedspace,
swap : item.swap,
hostname : item.hostname
}}}>{item.ip}</Link> :<div>{item.ip}</div>}
</TableCell>
                <TableCell align="center">{item.hostname}</TableCell>
                <TableCell align="center">{item.usedspace}</TableCell>
                <TableCell align="center">{item.ram}</TableCell>
                <TableCell align="center">{item.swap}</TableCell>
                <TableCell align="center">{item.os}</TableCell>
                <TableCell align="center">{item.osversion}</TableCell>
                <TableCell align="center">{item.project}</TableCell>
                <TableCell align="center">{item.env}</TableCell>
                
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
     
    );
  }
 }

 export default DataTable;