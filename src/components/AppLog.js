import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import { Redirect } from  'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import {API_URL,getToken} from './constants';


class AppLog extends React.Component{
    constructor(props){
        super(props);
        this.state={
            ip:'',
            appitems:[],
            logitems:[],
            appstate:true,
            logstate:true,
            path:'',
            return:false
        }
    }

    
    handleDelete = (event) =>{
      event.preventDefault();
      //console.log(this.state.path)
      this.setState({open:!this.state.open}) 
      let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
      axios.delete(API_URL+"/deletepath?ip="+this.state.ip+"&path="+this.state.path, {headers: {'Authorization': AuthStr}})
      .then(res => {
        window.location.reload(true);
          if(res.status === 200){
          this.setState({isdeleted:!this.state.isdeleted})
          this.setState({message:res.data.data[0].message}) 
        }
        else if(res.status === 201){
            this.setState({message:res.data.message})
        }
      })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    }

    handleDailog = parameter => event =>{
        event.preventDefault();
        //console.log(parameter)
        this.setState({path:parameter})
        this.setState({open:!this.state.open}) 
      }
      handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }

   componentDidMount(){
    this.state.ip=this.props.location.state.ip;
    let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/applog/'+this.state.ip, { headers: {'Authorization': AuthStr}})
    .then(res => {
        //console.log(res.data.data.app)
        if(res.status === 200){
            let appsorteditems = [...res.data.data.app]
            let sortedField = "filesize"
            appsorteditems.sort((a,b)=>{ if (a[sortedField] < b[sortedField]) {
                return 1;
              }
              if (a[sortedField] > b[sortedField]) {
                return -1;
              }
              return 0;
            });
            this.setState({appitems:appsorteditems})
            //console.log(appsorteditems)


            if(this.state.appitems.length === 0){
                this.setState({appstate:!this.state.appstate})
            }
            let logsorteditems = [...res.data.data.log]
            logsorteditems.sort((a,b)=>{ if (a[sortedField] < b[sortedField]) {
                return -1;
              }
              if (a[sortedField] > b[sortedField]) {
                return 1;
              }
              return 0;
            });
            this.setState({logitems:logsorteditems})
            //console.log(logsorteditems)

            if(this.state.logitems.length === 0){
                this.setState({logstate:!this.state.logstate})
            }

        }
    })
    .catch(error => {
      if(error.response){
        //console.log(error.response)
        this.setState({return:!this.state.return})
    }
      }) 
   }


    
    render(){
        var {appitems,logitems}=this.state;
        if(this.state.return){
          return <Redirect to="/signin"/>
      }
      return(
        <TableContainer>
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               {this.state.ip}  
        </Typography>
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Mount Point : /app  
        </Typography>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>Path</TableCell>
              <TableCell align="center">Size&nbsp;</TableCell>
              <TableCell align="center">Delete&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {appitems
            .map((item,index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {item.path}
                </TableCell>
                <TableCell align="center">{item.filesize}</TableCell>  
                <TableCell align="center">
                {/* <DailogOpen ip={item.ip} /> */}
              <DeleteIcon fontSize="small" onClick={this.handleDailog(item.path)}/>
              </TableCell>
              <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Status"}</DialogTitle>
          <DialogContent>
            
            <DialogContentText>
            Do You Want To Delete The Files In The Path :  {this.state.path}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         <Button onClick={this.handleClose} color="primary">
             No
          </Button>
          <Button onClick={this.handleDelete} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>             
              </TableRow>
            ))}
          </TableBody>
          <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Mount Point : /log  
        </Typography>
        </Table>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>Path</TableCell>
              <TableCell align="center">Size&nbsp;</TableCell>
              <TableCell align="center">Delete&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {logitems
            .map((item,index) => (
              <TableRow key={item.path}>
                <TableCell component="th" scope="row">
                  {item.path}
                </TableCell>
                <TableCell align="center">{item.filesize}</TableCell>  
                <TableCell align="center">
                {/* <DailogOpen ip={item.ip} /> */}
              <DeleteIcon fontSize="small" onClick={this.handleDailog(item.path)}/>
              </TableCell>
              <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Status"}</DialogTitle>
          <DialogContent>
            
            <DialogContentText>
            Do You Want To Delete The Files In The Path :  {this.state.path}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         <Button onClick={this.handleClose} color="primary">
             No
          </Button>
          <Button onClick={this.handleDelete} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>               
              </TableRow>
            ))}
          </TableBody>
        </Table>
        </TableContainer>
      )}}

export default AppLog;