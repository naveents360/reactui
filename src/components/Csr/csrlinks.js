import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link, Redirect } from  'react-router-dom';
import {API_URL,getToken,verifyheader} from './../constants';
import axios from 'axios';

class csrlinks extends React.Component{
    constructor(props){
        super(props);
        this.state={
        return:'',
      }
    }
    
      componentDidMount(){
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/testdecodejwt', { headers: {'Authorization': AuthStr}})      
        .then(response => {
            return response.status
         })
        .catch((error) => {
            if(error.response){
           this.setState({return:!this.state.return})
            }
         });
      }
        render(){
          if(this.state.return){
            return <Redirect to="/signin"/>
        }
        return(            
            <Container component="main" maxWidth="xs"style={{textAlign:"center"}}>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                  CSR
            </Typography>
            <Button><Link to="/generatecsr">Generate CSR</Link></Button>
            <br />
            <Button><Link to="/allcsrs">Available CSRs</Link></Button>
            <br />
            {/* <Button><Link target={"_blank"} to="http://hpsmbss.oss.jio.com/smbss/index.do">HPSM</Link></Button> */}
           {/* <Button> <a rel="noopener noreferrer" href="http://hpsmbss.oss.jio.com/smbss/index.do" target="_blank">Link Here</a></Button> */}
            <br/>
            </Container>
        )
      }

}

export default csrlinks;


