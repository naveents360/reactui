import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken} from './../constants';
import { Redirect } from  'react-router-dom';


class IndividualCsr extends React.Component{
    constructor(props){
        super(props);
        this.state={
        fqdn:'',
        email:'',
        project:'',
        state:'',
        location:'',
        san:'',
        userhelptext:'',
        iphelptext:'',

        env:'',
        open:false,
        loading:false,
        projects:[],
        return:false,
        csr:[],
        key:[],
        output:false
        }     
       }

       componentDidMount(){
           this.state.fqdn = this.props.location.state.fqdn
        this.setState({fqdn : this.props.location.state.fqdn})
        this.setState({project : this.props.location.state.project})
        this.setState({san : this.props.location.state.san})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getindividualcsrs/'+this.state.fqdn, { headers: { 'Authorization': AuthStr } })
 .then(response => {
     // If request is good...
     //console.log(response.data.message.csr)
     console.log(response.data.message)
     this.setState({email:response.data.message.email})
     this.setState({state:response.data.message.state})
     this.setState({location:response.data.message.location})
     this.setState({project:response.data.message.project})
     this.setState({csr:response.data.message.csr})
     this.setState({key:response.data.message.key})
     this.setState({output:!this.state.output})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }        
    render(){
      var {csr}=this.state;
      var {key}=this.state;
      //const disabled = !this.state.fqdn.length || !this.state.email.length || !this.state.project.length || !this.state.state.length || !this.state.location.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               {this.state.fqdn}
        </Typography>
        <form  noValidate>
          <TextField           
          disabled = "True"
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Common Name"
            name="common Name"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.fqdn}          
          />
          <br/>
          <TextField
          disabled = "True"
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Email"
            name="email"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.email}
          />
          <br />
          <TextField
          disabled = "True"
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Project"
            name="email"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.project}
          />
          <br />

 
      <TextField
      disabled = "True"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="state"
              label="State"
              onChange = {this.handleChangepassword}
              value = {this.state.state}
              helperText = {this.state.passhelpertext}
              />
           <br/>
           <TextField
              variant="outlined"
              disabled = "True"
              margin="normal"
              required
              fullWidth
              name="state"
              label="Location"
              onChange = {this.handlechangelocation}
              value = {this.state.location}
              helperText = {this.state.passhelpertext}
              />
           <br/>
           <TextField
           disabled = "True"
              variant="outlined"
              margin="normal"
              fullWidth
              name="san"
              label="SAN(Optional)"
              onChange = {this.handlechangesan}
              value = {this.state.san}
              helperText = {this.state.passhelpertext}
              />
            {/* Eg:- DNS:dns1.jio.com & DNS:dns2.jio.com
           <br/>           
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            GenerateCsr            
           </Button>       */}
            <br/>     
            {this.state.output && <p>CSR:-</p>}    
            {csr &&            
             csr.map((data,index)=>
               <div key={index}>                
               {data}</div>
               )               
           } 
           {this.state.output && <p>KEY:-</p>}  
           {key && key.map((data,index)=>
               <div key={index}>
               {data}</div>
               )
           }    
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default IndividualCsr;