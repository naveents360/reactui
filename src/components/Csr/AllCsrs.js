import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import {API_URL,getToken} from './../constants'
import { Link, Redirect } from  'react-router-dom';
import Button from '@material-ui/core/Button';

class AllCsrs extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      return:false,
  }
  }
  
  downloadKey = parameter =>{
    //console.log(parameter)
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.get(API_URL+'/downloadfile/'+parameter+'.key',{ headers: {'Authorization': AuthStr}})
            .then(response => {
              if(response.status === 200){
              //alert(res.data.data[0].message);
              const url = window.URL.createObjectURL(new Blob([response.data]));
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", parameter+'.key');
              document.body.appendChild(link);
              link.click();
              link.remove();
            }
            if(response.status === 201){
              this.setState({message:response.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              //console.log(error.response)
              this.setState({return:!this.state.return})
          }
            })     
        
  }

 
  downloadCsr = parameter =>{
    //console.log(parameter)
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.get(API_URL+'/downloadfile/'+parameter+'.csr',{ headers: {'Authorization': AuthStr}})
            .then(response => {
              if(response.status === 200){
              //alert(res.data.data[0].message);
              const url = window.URL.createObjectURL(new Blob([response.data]));
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", parameter+'.csr');
              document.body.appendChild(link);
              link.click();
              link.remove();
            }
            if(response.status === 201){
              this.setState({message:response.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              //console.log(error.response)
              this.setState({return:!this.state.return})
          }
            })     
        
  }
    componentDidMount() {
      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN); 

      axios.get(API_URL+'/getallcsrs', { headers: {'Authorization': AuthStr}})
        .then(res => {
          if(res.status === 200){
          //alert(res.data.data[0].message);
          this.setState({items:res.data.message})
          let sortedField="project";
          let sorteditems = [...res.data.data];
          sorteditems.sort((a,b)=>{
             if (a[sortedField] < b[sortedField]) {
            return -1;
          }
          if (a[sortedField] > b[sortedField]) {
            return 1;
          }
          return 0;
        });
        this.setState({items:sorteditems})
        }
              })
      .catch(error => {
        if(error.response){
          //console.log(error.response)
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    var {items}=this.state;
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
  return (
      
    <TableContainer>
      <CssBaseline />
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
          <TableCell>Sl.No</TableCell>
            <TableCell>CommonName</TableCell>
            <TableCell align="center">SAN</TableCell>
            {/* <TableCell align="center">Password&nbsp;</TableCell> */}
            <TableCell align="center">Project&nbsp;</TableCell>
            <TableCell align="center">Created By&nbsp;</TableCell>
            <TableCell align="center">View details&nbsp;</TableCell>
            <TableCell align="center">CSR</TableCell>
            <TableCell align="center">KEY</TableCell>
            {/* <TableCell align="center">Delete&nbsp;</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {items
          .map((item,index) => (
            <TableRow key={item.ip}>
            <TableCell>{index+1}</TableCell>
              <TableCell component="th" scope="row">
                {item.fqdn}
              </TableCell>
              <TableCell align="center">{item.san}</TableCell>
              {/* <TableCell align="center">{item.password}</TableCell> */}
              <TableCell align="center">{item.project}</TableCell>
              <TableCell align="center">{item.username}</TableCell>       
              <TableCell align="center">
              <Link to={{pathname:"/individualcsr",  state:{
                  fqdn : item.fqdn,
                  san : item.san,
                  project:item.project
              }}}>View Details</Link>           
            </TableCell>     
            <TableCell align="center">
            <Button
            
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={() => this.downloadCsr(item.fqdn)}
            >
            Download          
           </Button> 
           </TableCell>
           <TableCell align="center">
            <Button
            
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={() => this.downloadKey(item.fqdn)}
            >
            Download         
           </Button> 
           </TableCell>
            {/* <TableCell align="center">
            <DeleteIcon fontSize="small" onClick={this.handleDailog(item.ip)}/>
            </TableCell> */}
  
            
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    
  );
  }
 }

 export default AllCsrs;