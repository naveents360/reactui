import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL, getToken} from './../constants';
import { Redirect } from  'react-router-dom';


class GenerateCsr extends React.Component{
    constructor(props){
        super(props);
        this.state={
        fqdn:'',
        email:'',
        project:'',
        state:'',
        location:'',
        san:'',
        userhelptext:'',
        iphelptext:'',
        env:'',
        open:false,
        loading:false,
        projects:[],
        return:false,
        csr:[],
        key:[],
        output:false
        }     
       }
       componentDidMount(){
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        axios.get(API_URL+'/getprojectlist', { headers: { 'Authorization': AuthStr } })
 .then(response => {
     // If request is good...
     //console.log(response.data)
     this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }


       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }
       handlechangelocation = event =>{
        event.preventDefault();
        this.setState({location: event.target.value})
    }

       handleProject = event =>{
        event.preventDefault();
        //console.log(event.target.value)
        this.setState({project: event.target.value})
    }

       handleChangeserverip = event =>{
           event.preventDefault();
           this.setState({fqdn: event.target.value})
       }
       handleChangeusername= event =>{
           event.preventDefault();
           this.setState({email:event.target.value})
       }
       handleChangepassword = event=>{          
           event.preventDefault();
           this.setState({state:event.target.value})
           }
           handlechangesan = event=>{          
            event.preventDefault();
            this.setState({san:event.target.value})
            }
        

       validate = ()=>{
           let userhelptext = "";
           let passhelpertext = "";
           let iphelptext = "";
           const ip = this.state.serverip;
            if(!this.state.password){
                passhelpertext = "Password Should Not Be Empty";
            }
              if(!this.state.username){
                userhelptext = "Username Should Not Be Empty";
             }
        const expression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
          if(!expression.test(ip)){
            iphelptext = "Invalid IP";
          }
            if(userhelptext || iphelptext ||  passhelpertext){
               this.setState({userhelptext,iphelptext,passhelpertext});
               return false;
           }
           return true;

       }
      
        handleClick(event){
        event.preventDefault();
        this.setState({open: !this.state.open })
        this.setState({loading: !this.state.loading})
        if(this.state.state == "Karnataka"){
          console.log("here")
          var location = "Banglore"
        }else{
          var location = "Mumbai"
        }

            const payload = {
               "fqdn":this.state.fqdn,
               "email":this.state.email,
               "state":this.state.state,
               "project":this.state.project,
               "location":location,
               "san":this.state.san
           } 
        //const isvalid = this.validate();
                
        this.setState({message:''})
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
        const options = {
          headers: {
              'Authorization': AuthStr,
          }
        };
        axios.post(API_URL+'/generatecsr', payload,options)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
            //console.log(res.data.message)
            this.setState({csr:res.data.message.csr})           
            this.setState({key:res.data.message.key})
            this.setState({output:!this.state.output})
            this.setState({open:!this.state.open})
            //console.log(this.state.csr)
          }
          else if(res.status === 201){
              //console.log(res.data.message);
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            //console.log(error.response)
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({key:'',san:'',fqdn:'',email:'',project:'',state:'',location:'',userhelptext:'',iphelptext:'',remoteiphelptext:'',passhelpertext:'',csr:''})
        }

        
    render(){
      var {csr}=this.state;
      var {key}=this.state;
      const disabled = !this.state.fqdn.length || !this.state.email.length || !this.state.project.length || !this.state.state.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Generate Csr
        </Typography>
        <form  noValidate>
          <TextField           
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Common Name"
            name="common Name"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.fqdn}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Email"
            name="email"     
            onChange = {this.handleChangeusername}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.email}
          />
          

    <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Project</InputLabel>
        <Select          
          value={this.state.project}
          onChange={this.handleProject}
          label="Project"
        >
          {
            this.state.projects.map((project,index)=>
            <MenuItem key={index} value={project}>{project}</MenuItem>)
          }
        </Select>
      </FormControl>
      {/* <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="state"
              label="State"
              onChange = {this.handleChangepassword}
              value = {this.state.state}
              //helperText = {this.state.passhelpertext}
              /> */}
               <FormControl variant="outlined" margin="normal" required
          fullWidth>
    <InputLabel>State</InputLabel>
        <Select          
          value={this.state.state}
          onChange={this.handleChangepassword}
          label="State"
        >
          <MenuItem value="Karnataka">Karnataka</MenuItem>
          <MenuItem value="Maharastra">Maharastra</MenuItem>
          {/* <MenuItem value="REPLICA">REPLICA</MenuItem>     */}
        </Select>
      </FormControl>
           {/* <br/>
           <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="state"
              label="Location"
              onChange = {this.handlechangelocation}
              value = {this.state.location}
              helperText = {this.state.passhelpertext}
              /> */}
           <br/>
           <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              name="san"
              label="SAN(Optional)"
              onChange = {this.handlechangesan}
              value = {this.state.san}
              //helperText = {this.state.passhelpertext}
              />
            Eg:- DNS:dns1.jio.com & DNS:dns2.jio.com
           <br/>           
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            GenerateCsr            
           </Button>      
            <br/>     
            {this.state.output && <p>CSR:-</p>}    
            {csr &&            
             csr.map((data)=>
               <div>                
               {data}</div>
               )               
           } 
           {this.state.output && <p>KEY:-</p>}  
           {key && key.map((data)=>
               <div>
               {data}</div>
               )
           }    
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default GenerateCsr;