import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import { Redirect } from  'react-router-dom';
import {API_URL,getToken} from './../constants'
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';


class ProjectMonitorData extends React.Component{
    constructor(props){
        super(props);
        this.state={
          items:  [],
          isloaded: false,
          projectname:'',
          message:'',
          port:'',
          ip:'',
          return:false
      }
      }

      handleDailog = parameter=>event =>{
        event.preventDefault();
        this.state.items.forEach(element => {
            if (element.id === parameter){
                this.setState({ip:element.ip})
                this.setState({port:element.port})
            }            
        });
        this.setState({id:parameter})        
        this.setState({open:!this.state.open}) 
      }

      handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }

      handleDelete = (event) =>{
        event.preventDefault();
        this.setState({open:!this.state.open}) 
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN);
        axios.delete(API_URL+"/deleteportmonitor?id="+this.state.id,{headers: {'Authorization': AuthStr}})
        .then(res => {
          window.location.reload(true);
            if(res.status === 200){
            this.setState({isdeleted:!this.state.isdeleted})
            this.setState({message:res.data.data[0].message})
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
      }
          
      // componentDidUpdate(prevProps){
      //   //console.log(prevProps)
      //   console.log(prevProps.location.state.projectname.project)
      //   console.log(this.state.projectname)
      //   if (prevProps.location.state.projectname.project !== this.state.projectname){
      //    this.setState({projectname:prevProps.location.state.projectname.project})
      //   }
      // }

        componentDidMount() {
          
          this.state.projectname=this.props.location.state.projectname
          console.log(this.state.projectname)
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN);
          axios.get(API_URL+'/monitorstatus/'+this.state.projectname.project, { headers: {'Authorization': AuthStr}})
            .then(res => {
              if(res.status === 200){
              let sortedField="status";
              let sorteditems = [...res.data.data];
              sorteditems.sort((a,b)=>{ if (a[sortedField] < b[sortedField]) {
                return -1;
              }
              if (a[sortedField] > b[sortedField]) {
                return 1;
              }
              return 0;
            });
            this.setState({items:sorteditems})
            }
            if(res.status === 201){
              this.setState({message:res.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              this.setState({return:!this.state.return})
          }
            })     
        
        }
      render() {
        var {items}=this.state;
        if(this.state.return){
          return <Redirect to="/signin"/>
      }
        return (
          
          <TableContainer>
              <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Port Monitoring on Project: {this.state.projectname.project}
                   </Typography>    
            <CssBaseline />
            <Table aria-label="customized table">
              <TableHead>
                <TableRow>
                 <TableCell>Sl.No</TableCell>
                  <TableCell align="center">IP</TableCell>
                  <TableCell align="center">Port&nbsp;</TableCell>
                  <TableCell align="center">Pid&nbsp;</TableCell>
                  <TableCell align="center">Status&nbsp;</TableCell>
                  <TableCell align="center">Stop Monitoring&nbsp;</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {items.map((item, index) => (
                  <TableRow key={item.id}>
                  <TableCell>{index+1}</TableCell>
                  <TableCell align="center">{item.ip}</TableCell>
                    <TableCell align="center">{item.port}</TableCell>
                    <TableCell align="center">{item.pid}</TableCell>
                    <TableCell align="center">{item.status}</TableCell>     
                    <TableCell align="center">
              <DeleteIcon fontSize="small" onClick={this.handleDailog(item.id)}/>
              </TableCell>             
              <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            Do You Want To Stop Monitoring port {this.state.port} on {this.state.ip}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         <Button onClick={this.handleClose} color="primary">
             No
          </Button>
          <Button onClick={this.handleDelete} color="primary">
             Yes
          </Button>
          </DialogActions>
        </Dialog>  
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        );
      }
     }
    
export default ProjectMonitorData;