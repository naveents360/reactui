import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,getToken,validateipaddress,validateportnumber} from './../constants'

class PoertinfoOnboardedServer extends React.Component{
    constructor(props){
        super(props);
        this.state={
        serverip:'',
        Port:'',
        message:'',
        iphelptext:'',
        porthelptext:'',
        open:false,
        loading:false,
        }
       }
       handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }
       handleChangeserverip = event =>{
           event.preventDefault();
           this.setState({serverip: event.target.value})
       }
       handleChangeportnumber = event =>{
           this.setState({Port:event.target.value})
           event.preventDefault();  
       }

       validate = ()=>{
        var ip = this.state.serverip;
        var port = this.state.Port
        let iphelptext = validateipaddress(ip)
        let porthelptext = validateportnumber(port) 
            if( iphelptext || porthelptext){
              this.setState({iphelptext,porthelptext});
               return false;
           }
           return true;
       }
        handleClick(event){
        event.preventDefault();
        
            const payload = {
               "port":this.state.Port,
               "ip":this.state.serverip,
           } 
        const isvalid = this.validate();
        if(isvalid){    
          this.setState({open: !this.state.open })
        this.setState({loading: !this.state.loading})
        this.setState({message:''})    
        let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN);
        axios.post(API_URL+'/portinfoonboardedserver', payload,{headers: {'Authorization': AuthStr}})
        .then(res => {
            this.setState({loading:!this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.data[0].message})
          }
          else if(res.status === 201){
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({Port:'',serverip:'',iphelptext:''})
        }
        
         }
    render(){
        const disabled = !this.state.serverip.length || !this.state.Port.length;
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               PortInfo OnBoarded Server
        </Typography>
        <form  noValidate>
          <TextField           
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Server IP Address"
            name="serverip"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.serverip}          
          />
            <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Port Number"
            name="Port Number"
            autoFocus
            onChange = {this.handleChangeportnumber}
            helperText = {this.state.porthelptext}
            value={this.state.Port}
          />
           <br/>
          <Button
            disabled={disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}            
            >
            Submit            
           </Button>
          <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary">
             OK
            </Button>}
          </DialogActions>
        </Dialog>         
            
        </form>
        </Container>
    )

}
}
export default PoertinfoOnboardedServer;