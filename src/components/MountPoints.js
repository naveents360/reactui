import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Link } from  'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,getToken,encryptstring} from './constants';
import {Redirect } from  'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  chip: {
    margin: theme.spacing(0.5),
  },
  section1: {
    margin: theme.spacing(3, 2),
  },
  section2: {
    margin: theme.spacing(2),
  },
  section3: {
    margin: theme.spacing(3, 1, 1),
  },
}));

class MountPoints extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      open:false,
      ip:'',
      ram:'',
      swap:'',
      usedspace:'',
      hostname:'',
      appstate:false,
      loading:false,
      message:'',
      return:false,
      home:false
  }
  }

  downloadexcelfile = () =>{
    //console.log(parameter)
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.get(API_URL+'/generateutilizationreport?ip='+encryptstring(this.state.ip),{ headers: {'Authorization': AuthStr},responseType: 'arraybuffer'})
            .then(response => {
              if(response.status === 200){
              //alert(res.data.data[0].message);
              var data = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
              const url = window.URL.createObjectURL(data);
              console.log(url)
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", this.state.ip+'.xlsx');
              document.body.appendChild(link);
              link.click();
              link.remove();
            }
            if(response.status === 201){
              this.setState({message:response.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              //console.log(error.response)
              this.setState({return:!this.state.return})
          }
            })     
        
  }

  handleHomeButton = event=>{
    event.preventDefault();
    this.setState({home:!this.state.home})
  }
    
  handleClose = event  =>{
    event.preventDefault();
    this.setState({open:!this.state.open})
  
  }

  handleRefreshClick = event => {
    event.preventDefault();
    let TOKEN = getToken();
        const AuthStr = 'Bearer '.concat(TOKEN); 
       axios.get(API_URL+'/refreshresourcedata?ip='+this.state.ip, { headers: {'Authorization': AuthStr}})      
 .then(response => {
        //console.log(response.data[0])
        this.setState({ram:response.data[0].ram})
        this.setState({swap:response.data[0].swap})
        this.setState({usedspace:response.data[0].usedspace})
        this.setState({hostname:response.data[0].hostname})
 })
  }

    componentDidMount() {
      this.setState({loading:!this.state.loading})
      this.setState({open: !this.state.open })
      this.setState({ip : this.props.match.params.ip})
      this.setState({ram: this.props.location.state.ram})
      this.setState({swap : this.props.location.state.swap})
      this.setState({usedspace: this.props.location.state.usedspace}) 
      this.setState({hostname:this.props.location.state.hostname})
     // this.setState({ip : this.props.location.state.ip},()=>{console.log(this.state.ip)})

      let TOKEN = getToken();
      const AuthStr = 'Bearer '.concat(TOKEN);
      axios.get(API_URL+'/mountpoints?ip='+this.props.location.state.ip,{ headers: {'Authorization': AuthStr}})
        .then(res => {
          this.setState({loading:!this.state.loading})          
          if(res.status === 200){
        this.setState({items:res.data.data})
        if (this.state.items.length === 0){
          this.setState({message:"Password Related Issue"})
          this.setState({open: !this.state.open })          
        }
        this.setState({open: !this.state.open })
        let filteritems = [...res.data.data];
        filteritems.forEach((element)=>{
           
            if(element.mount.toString() === "/app")  {
                  this.setState({appstate:!this.state.appstate})        
            }
           })
            
        }
              })
      .catch(error => {
        if(error.response){
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    if(this.state.return){
      return <Redirect to="/signin"/>
  }
  if(this.state.home){
    return <Redirect to="/home"/>
}
    var {items}=this.state;
    //const classes = useStyles();

    return (
      
      <TableContainer>
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              IP:- {this.state.ip}  
        </Typography>
        <Divider  fullwidth />
        <Grid container spacing={30}>
        <Grid item xs={2}>
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="RAM"
            name="ram"    
            value={this.state.ram}          
          />
          </Grid>
          <Grid item xs={2}>
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="SWAP"
            name="swap"    
            value={this.state.swap}          
          />
          </Grid>
          <Grid item xs={2}>
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="UsedSpace"
            name="usedspace"    
            value={this.state.usedspace}          
          />
          </Grid>
          <Grid item xs={2}>
           <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="Hostname"
            name="hostname"    
            value={this.state.hostname}          
          />
          </Grid>
          </Grid>
          <br/>
          <Grid container spacing={1}>
          <Grid item xs={1.25}>
          <Link  to={{pathname:"/portlist",  state:{
                  ip : this.state.ip,
                   
                }}}>
                
          <Button
          //style="margin:5px"
          type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            Ports           
           </Button>   
           </Link>   
           </Grid>
           
           <Grid item xs={1.25}>
          <Link  to={{pathname:"/executecommand",  state:{
                  ip : this.state.ip,
                   
                }}}>
          <Button
          //style="margin:5px"
            type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            Execute Command           
           </Button>   
           </Link>   
           </Grid>
           <Grid item xs={1.25}>
           <Link  to={{pathname:"/testtelnet",  state:{
                  ip : this.state.ip,
                   
                }}}>
          <Button
          //style="margin:5px"
            type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            Test Connectivity          
           </Button>   
           </Link>   
           </Grid>
           <Grid item xs={1.25}>
           <Link  to={{pathname:"/uploadfile",  state:{
                  ip : this.state.ip,
                   
                }}}>
          <Button

            type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
             Upload  File        
           </Button>   
           </Link>   
           </Grid>
           <Grid item xs={1.5}>
             <Link  to={{pathname:"/installjava",  state:{
                  ip : this.state.ip,
                  version: ""
                   
                }}}>
                
          <Button

            type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            Install Java          
           </Button>   
           </Link>   
           </Grid>
           <Grid item xs={1.25}>
             <Link  to={{pathname:"/listfoldersmainpage",  state:{
                  ip : this.state.ip,                   
                }}}>                
          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="medium"
            >
            Access Folders        
           </Button>   
           </Link>   
           </Grid>
           <Grid item xs={1.25}>
           <Button
            
            type="submit"
            variant="contained"
            color="primary"
            onClick={() => this.downloadexcelfile()}
            >
            Utilization Report         
           </Button> 
           </Grid>
           {/* <Button
            //disabled = {disabled}
            type="submit"
            //fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleRefreshClick(event)}
            >
            Refresh            
           </Button> */}
           <Grid item xs={1.25}>
           <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={(event) => this.handleHomeButton(event)}            
            >
            Home            
           </Button>
           </Grid>
           </Grid>
           <br/>
           <Divider  fullwidth />
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>FileSystem</TableCell>
              <TableCell align="center">Size&nbsp;</TableCell>
              <TableCell align="center">Used&nbsp;</TableCell>
              <TableCell align="center">Avail&nbsp;</TableCell>
              <TableCell align="center">Use&nbsp;</TableCell>
              <TableCell align="center">Mount Point&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items
            .map((item,index) => (
              <TableRow key={item.mount}>
                <TableCell component="th" scope="row">
                  {item.filesystem}
                </TableCell>
                <TableCell align="center">{item.size}</TableCell>
                <TableCell align="center">{item.used}</TableCell>
                <TableCell align="center">{item.avail}</TableCell>
                <TableCell align="center">{item.use}</TableCell>   
                {/* <TableCell align="center">{item.mount}</TableCell> */}
                {this.state.appstate ?  <TableCell align="center">

                <Link to={{pathname:"/applog",  state:{
                  ip : this.state.ip,
                   
                }}}>{item.mount}</Link>           
                </TableCell>  :    <TableCell align="center">{item.mount}</TableCell> }
                
                
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Dialog          
       open={this.state.open}
       onClose={this.handleClose}
       aria-labelledby="responsive-dialog-title"
     >
       <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
       <DialogContent>
         <DialogContentText>
         {this.state.loading &&  <CircularProgress/>}
         {this.state.message}
         </DialogContentText>
       </DialogContent>
       <DialogActions>
      { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
          OK
         </Button>}
       </DialogActions>
     </Dialog>
      </TableContainer>      
    );
  }
 }

 export default MountPoints;