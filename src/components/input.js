import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import axios from 'axios';
import Tooltip from '@material-ui/core/Tooltip';

class Input extends React.Component{
    constructor(props){
        super(props);
        this.state={
            created_by:'',
            service_type:'',
        }
    }

    handleChangeServiceType= event =>{
        event.preventDefault();
        this.setState({service_type:event.target.value})
    }

    handleChangecreatedby = event => {
        event.preventDefault();
        this.setState({created_by:event.target.value})
    }

    handleClick(event){
        event.preventDefault();
        const payload={  
            "pipelineId":"demo/raise_request.yaml",
            input:{
        "requestor_site_name":"PIM",
        "table_name": "jio_pim",
        "workflow_id":"123461",
        "service_type": "pimaccess",
        "action_type":"create",
        "data":"{\"raised_by\":\"vinay.kahar@ril.com\",\"request_status\":\"PENDING\"}",
        "field_name": "raised_by,request_status",
        "has_approval_data": "true",
        "approval_data": "{\"L1\":\"sachin6.chavan@ril.com,nikesh.bansod@ril.com\",\"L2\":\"Rameez.Mulla@ril.com\"}",
        "approval_data_field_name": "L1,L2",
        "approval_level": "L1,L2"
            },
           "created_by":this.state.created_by,
           "service_type":this.state.service_type,
        }

       
    }


    render(){
        const disabled = !this.state.created_by.length || !this.state.service_type.length
    return(
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}>
                Work Flow
            </Typography>
        <form noValidate>
             <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                required
                label="Created By"
                name="created_by"
                autoFocus
                onChange={this.handleChangecreatedby}
                value={this.state.created_by}
             ></TextField>
             <br/>
             <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                required
                label="Service Type"
                name="service_type"
                autoFocus
                onChange={this.handleChangeServiceType}
                helperText={this.state.service_type_helpertext}
                value={this.state.service_type}
             ></TextField>
             <br/>
            <Tooltip title="Submit">
            <Button  disabled={disabled} type="submit" fullWidth variant="contained" color="primary" onClick={(event)=> this.handleClick(event)}>Submit</Button>
            </Tooltip>
        </form>
        </Container>

    )

    }

}

export default Input;