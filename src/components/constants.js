import React from 'react';
import  { Redirect } from 'react-router-dom';
import axios from 'axios';
//import { func } from 'prop-types';

export const API_URL ="https://infraportal-api.jio.com:8443";

export function getloggedin(){
    const islogged = sessionStorage.getItem('isloggedin')
    return islogged;
}

export function getToken(){
    const TOKEN = sessionStorage.getItem('accesstoken')
    return TOKEN;
}

export function validateipaddress(ip){
    var iphelptext = ""
    //console.log(ip)
    const expression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
    if(!expression.test(ip)){
        iphelptext = "Invalid IP";
    }
    return iphelptext
}

export function validateportnumber(port){
    let porthelptext = ""
    if(!port){
        porthelptext = "port Should Not Be Empty";
    }
    port = parseInt(port)
    if(isNaN(port)){
        porthelptext = "Please Enter Only Positive Integer Values Between 1 to 65535"   
    }
    if(port < 1 || port > 65535){
        porthelptext = "Port Number Should be in between 1 to 65535"
    }
    return porthelptext
}

export function validatepassword(password){
    var passhelpertext =""
    const passexp = /((^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})))/;
    if(!passexp.test(password)){
            passhelpertext = "Password Should contain atleast one lower case alphabet,one upper case alphabet,one number and a special character";
          }
    if(!password){
            passhelpertext = "Password Should Not Be Empty";
        }
    return passhelpertext
}



export function validateusernamelength(username){
    let userhelptext = ""
    if(!username){
        userhelptext = "Username Should Not Be Empty";
     }
     return userhelptext
}

export function encryptdata(password){
    var crypto = require('crypto'),
    key = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
    iv = "AAAAAAAAAAAAAAAA";

    var cipher = crypto.createCipheriv('aes-256-cbc',key,iv);
    cipher.update(password,'binary','base64');
    return cipher.final('base64');

}

export function encryptstring(password){
    var encodedstring = btoa(password)
    return encodedstring
    }

export const TEAMLEAD = ['Member'];
export const ADMIN = ['Admin','TeamLead','Member'];
export const OPTIONS = ["Change Ownership"];

export function verifyheader(){
    let TOKEN = getToken();
    const AuthStr = 'Bearer '.concat(TOKEN); 
    axios.get(API_URL+'/testdecodejwt', { headers: {'Authorization': AuthStr}})      
    .then(response => {
        return response.status
     })
    .catch((error) => {
        if(error.response){
        return <Redirect to="/signin"/>   }
     });
}

export function downloadfile(filename){
    let TOKEN = getToken();
    console.log(filename)
    const AuthStr = 'Bearer '.concat(TOKEN);
    axios.get(API_URL+'/downloadsop/'+filename,{ headers: {'Authorization': AuthStr}})
            .then(response => {
              if(response.status === 200){
              //alert(res.data.data[0].message);
              const url = window.URL.createObjectURL(new Blob([response.data]));
              console.log(filename)
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", filename);
              document.body.appendChild(link);
              link.click();
              link.remove();
            }
            if(response.status === 201){
              this.setState({message:response.data.message})
            }

                  })
          .catch(error => {
            if(error.response){
              //console.log(error.response)
              this.setState({return:!this.state.return})
          }
            }) 
}