import React from 'react';

// import SignIn from './components/SignIn.js'
import { Redirect } from  'react-router-dom';
import Telnet from './components/Telnet/Telnet.js'
import PortInfo from './components/PortInfo/PortInfo.js' 
import onboard from './components/Onboard.js'
import Mountpoints from './components/MountPoints.js'
import datatable from './components/DataTable.js'
import StatusUpdate from './components/StatusUpdate.js'
import Input from './components/input.js'
//import ShowApprovalStatus from './components/ShowApprovalStatus.js'
import Home from './components/Home.js'
import ProjectData from './components/ProjectData.js'
import ProjectLink from './components/ProjectLinks'
import TelnetOnBoardedServer from './components/Telnet/TelnetOnboardedServer'
import TelnetLinks from './components/Telnet/TelnetLinks'
import PortInfoLinks from './components/PortInfo/PortInfoLinks'
import PortInfoOnboardedServer from './components/PortInfo/PortinfoOnboardedServer'
import AllServerCredentials from './components/Credentials/AllServerCredentials'
import UpdateCredentials from './components/Credentials/UpdateCredentials'
import CredentialsLinks from './components/Credentials/CredentialLinks'
import CredentialProjectData from './components/Credentials/CredentialProjectData'
import { BrowserRouter as Router, Route,Switch } from 'react-router-dom'; 
import DailogOpen from './components/DailogOpen';
import AppLog from './components/AppLog'
import PortList from './components/PortList'
import Uploadfile from './components/UploadFile'
import ExecuteCommand from './components/ExecuteCommand'
import TestTelnet from './components/Telnet/TelnetTest'
import OtherServicesLandingPage from './components/Other Services/OtherservicesLandingPage'
import OnboardPortMonitor from './components/PortMonitor/OnboardPortMonitor'
import ProjectMonitorLinks from './components/PortMonitor/ProjectMonitorLinks'
import ProjectMonitorData from './components/PortMonitor/PortMonitorData'
import EditorFile from './components/Editor/EditorFile'
import SignIn from './components/SignIn'
import AddUser from './components/AddUser'
import UserProjectLink from './components/Users/UserProjectLink'
import UserList from './components/Users/UserList'
import CsrLinks from './components/Csr/csrlinks'
import AllCsrs from './components/Csr/AllCsrs'
import Generatecsr from './components/Csr/GenerateCsr'
import IndividualCsr from './components/Csr/IndividualCsr'
import TestEditor from './components/TestEditor'
import Inventory from './components/Inventories/Inventory'
import CompleteUsers from './components/CompleteUsers'
import InstallJava from './components/JavaInstallation/InstallJava'
import PortOpeningLandingPage from './components/Other Services/Port Opening Request/LandingPage'
import RaiseNewConnectivityRequest from './components/Other Services/Port Opening Request/RaiseNewRequest'
import AllConnectivityTickets from './components/Other Services/Port Opening Request/AllTickets'
import IndividualConnectivityTicket from './components/Other Services/Port Opening Request/IndividualConnectivityTicket'
import UpdateIndividualConnectivityTicket from './components/Other Services/Port Opening Request/UpdateconnectivityRequest'
import MainPage from './components/Links and Sops/MainPage'
import Links from './components/Links and Sops/Links'
import SOPs from './components/Links and Sops/SOPs'
import ListFoldersMainPage from './components/Server Folder/ListFoldersMainPage'
import ListFolders from './components/Server Folder/ListFolders'
import CkEditorExampleComponent from './components/Editor'

class App extends React.Component{
    render(){
    return(
      <Router>
      <Switch> 
              <Route exact path='/listfolders' component={ListFolders}></Route>
              <Route exact path='/listfoldersmainpage' component={ListFoldersMainPage}></Route>
              <Route exact path='/telnet' component={Telnet}></Route> 
              <Route exact path='/sops' component={SOPs}></Route> 
              <Route exact path='/editor' component={CkEditorExampleComponent}></Route> 
              <Route exact path='/links' component={Links}></Route> 
              <Route exact path='/linksmainpage' component={MainPage}></Route> 
              <Route exact path='/updateconnectivityticket' component={UpdateIndividualConnectivityTicket}></Route> 
              <Route exact path='/alltickets' component={AllConnectivityTickets}></Route> 
              <Route exact path='/individualconnectivityticket' component={IndividualConnectivityTicket}></Route>
              <Route exact path='/raisenewconnectivityrequest' component={RaiseNewConnectivityRequest}></Route> 
              <Route exact path='/portinfo' component={PortInfo}></Route> 
              <Route exact path='/home' component={Home}></Route>
              <Route exact path='/onboard' component={onboard}></Route>
              <Route exact path='/mountpoints' component={Mountpoints}></Route>
              <Route exact path="/mountpoints/:ip" component={Mountpoints} />
              <Route exact path='/table' component={datatable}></Route>
              <Route exact path='/servercredentials' component={AllServerCredentials}></Route>
              <Route exact path='/individualservercredentials' component={UpdateCredentials}></Route>
              <Route exact path='/statusupdate' component={StatusUpdate}></Route>
              <Route exact path='/input' component={Input}></Route>
              {/* <Route exact path='/showupdate' component={ShowApprovalStatus}></Route> */}
              <Route exact path='/credentialprojectdata' component={CredentialProjectData}></Route>
              <Route exact path='/otherservicespage' component={OtherServicesLandingPage}></Route>
              <Route exact path='/projectdata' component={ProjectData}></Route>
              <Route exact path='/projectlink' component={ProjectLink}></Route>
              <Route exact path='/credentialslink' component={CredentialsLinks}></Route>
              <Route exact path='/telnetonboardedserver' component={TelnetOnBoardedServer}></Route>
              <Route exact path='/telnetlinks' component={TelnetLinks}></Route>
              <Route exact path='/portinfolinks' component={PortInfoLinks}></Route>
              <Route exact path='/portinfoonboardedserver' component={PortInfoOnboardedServer}></Route>
              <Route exact path='/dailog' component={DailogOpen}></Route>
              <Route exact path='/applog' component={AppLog}></Route>
              <Route exact path='/portlist' component={PortList}></Route>
              <Route exact path='/uploadfile' component={Uploadfile}></Route>
              <Route exact path='/executecommand' component={ExecuteCommand}></Route>
              <Route exact path='/testtelnet' component={TestTelnet}></Route>
              <Route exact path='/onboardportmonitor' component={OnboardPortMonitor}></Route>
              <Route exact path='/projectmonitorlink' component={ProjectMonitorLinks}></Route>
              <Route exact path='/projectmonitordata' component={ProjectMonitorData}></Route>
              <Route exact path='/editor' component={EditorFile}></Route>
              <Route exact path='/signin' component={SignIn}></Route>
              <Route exact path='/adduser' component={AddUser}></Route>
              <Route exact path='/users' component={UserProjectLink}></Route>
              <Route exact path='/userlist' component={UserList}></Route>
              <Route exact path='/allcsrs' component={AllCsrs}></Route>
              <Route exact path='/csrlinks' component={CsrLinks}></Route>
              <Route exact path='/generatecsr' component={Generatecsr}></Route>
              <Route exact path='/individualcsr' component={IndividualCsr}></Route>
              <Route exact path='/testeditor' component={TestEditor}></Route>
              <Route exact path='/inventory' component={Inventory}></Route>
              <Route exact path='/completeusers' component={CompleteUsers}></Route>
              <Route exact path='/installjava' component={InstallJava}></Route>
              <Route exact path='/Portopeninglandingpage' component={PortOpeningLandingPage}></Route>
              <Route render={() => <Redirect to="/home" />} />
      </Switch> 
      </Router>
    )
  }
}
  
export default App;
